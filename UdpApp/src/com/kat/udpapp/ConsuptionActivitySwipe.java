package com.kat.udpapp;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.example.udpapp.R;

public class ConsuptionActivitySwipe extends FragmentActivity implements
		OnClickListener {

	TextView txtHeaderTop, txtBack, txtHelp;

	private String password, myIP;

	private InetAddress controllerAddress;
	private InetAddress myAddress;
	private NBEControllerClient client;

	ViewPager viewPager;
	PagerTabStrip strip;

	View v;

	LinearLayout main_container;
	

	Toast toast;

	LinearLayout llBack;

	RadioGroup rgp;
	SparseArray<RadioButton> buttonID;
	 WakeLock wakeLock;
	static String[] totalHourArray = { "40", "33", "50", "25", "40", "33",
			"50", "25", "40", "33", "50", "25", "40", "33", "50", "25", "40",
			"33", "50", "25", "40", "33", "50", "25" }, dhwHourArray = { "50",
			"20", "42", "18", "15", "20", "50", "18", "15", "20", "42", "18",
			"49", "20", "42", "50", "15", "20", "42", "18", "15", "20", "42",
			"18" };
	static String[] totalDaysArray = { "40", "33", "50", "25" },
			dhwDaysArray = { "15", "20", "42", "18" };
	static String[] totalMonthsArray = { "1410", "752", "621", "1000" },
			dhwMonthsArray = { "1410", "552", "901", "900" };
	static String[] totalYearsArray = { "5200", "6522", "8500", "9000",
			"11000", "13505", "14999" }, dhwYearsArray = { "5000", "7522",
			"8500", "10000", "12000", "11505", "7600" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		setContentView(R.layout.activity_second);

		v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.consumption_activity_swipe, null, false);
		v.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		viewPager = (ViewPager) v.findViewById(R.id.pager);
		strip = (PagerTabStrip) v.findViewById(R.id.strip);
		rgp = (RadioGroup) v.findViewById(R.id.radioGroup1);

		buttonID = new SparseArray<RadioButton>();
		buttonID.put(0, (RadioButton) rgp.getChildAt(0));
		buttonID.put(1, (RadioButton) rgp.getChildAt(1));
		buttonID.put(2, (RadioButton) rgp.getChildAt(2));
		buttonID.put(3, (RadioButton) rgp.getChildAt(3));

		for (int i = 0; i < strip.getChildCount(); ++i) {
			View nextChild = strip.getChildAt(i);
			if (nextChild instanceof TextView) {
				TextView textViewToConvert = (TextView) nextChild;
				Typeface tf = Typeface.createFromAsset(this.getAssets(),
						"OPENSANS-LIGHT.TTF");
				textViewToConvert.setTypeface(tf);
				textViewToConvert.setTextSize(getResources().getDimension(
						R.dimen.onezero));
			}
		}

		viewPager.setBackgroundColor(this.getResources().getColor(
				android.R.color.transparent));

		password = Constants.password;
		try {
			controllerAddress = InetAddress.getByName(Constants.controller);
			myIP = getLocalIpAddress();
			myAddress = InetAddress.getByName(myIP);
			client = new NBEControllerClient();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}

		txtHelp = (TextView) v.findViewById(R.id.txtHelp);
		txtBack = (TextView) v.findViewById(R.id.txtBack);
		txtHeaderTop = (TextView) v.findViewById(R.id.txtHeaderTop);
		llBack = (LinearLayout) v.findViewById(R.id.llBack);

		new getConsuptionData().execute();

		llBack.setOnClickListener(ConsuptionActivitySwipe.this);

		SharedPreferences preferences = getSharedPreferences(
				Constants.language, Context.MODE_PRIVATE);

		txtBack.setText(preferences.getString("lng_button_back", ""));
		txtHelp.setText(preferences.getString("lng_button_help", ""));
		txtHeaderTop.setText(preferences
				.getString("lng_button_consumption", ""));

		try {
			main_container = (LinearLayout) findViewById(R.id.main_container);
			main_container.addView(v);
		} catch (Exception e) {
			e.printStackTrace();
		}

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				rgp.check((buttonID.get(position)).getId());
			}

			@Override
			public void onPageScrolled(int position, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		  PowerManager powerManager =
			        (PowerManager)getSystemService(Context.POWER_SERVICE);
			       wakeLock =
			        powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
			          "Full Wake Lock");

	}

	public String getLocalIpAddress() {
		try {
			String ip = null;
			ConnectivityManager connManager = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			WifiManager wifiMan = (WifiManager) this
					.getSystemService(Context.WIFI_SERVICE);
			if (wifiMan.isWifiEnabled()) {
				WifiInfo wifiInf = wifiMan.getConnectionInfo();

				int ipAddress = wifiInf.getIpAddress();
				ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
						(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
						(ipAddress >> 24 & 0xff));
				return ip;
			} else if ((netInfo != null) && netInfo.isConnected()) {
				for (Enumeration<NetworkInterface> en = NetworkInterface
						.getNetworkInterfaces(); en.hasMoreElements();) {
					NetworkInterface intf = en.nextElement();
					for (Enumeration<InetAddress> enumIpAddr = intf
							.getInetAddresses(); enumIpAddr.hasMoreElements();) {
						InetAddress inetAddress = enumIpAddr.nextElement();
						if (!inetAddress.isLoopbackAddress()) {
							ip = inetAddress.getHostAddress().toString();
						}
					}
				}
				return ip;
			}
		} catch (Exception ex) {
			Log.e("IP Address", ex.toString());
		}
		return null;
	}

	public class getConsuptionData extends AsyncTask<Void, Void, Void> {

		ControllerResponse response;
		ControllerRequest cr;
		Map<String, String> map;
		boolean error = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			toast = Toast.makeText(ConsuptionActivitySwipe.this,
					"Loading Charts", Toast.LENGTH_SHORT);
			toast.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			
			try {
				error = false;
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_hours");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());

					// totalHourArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_days");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// totalDaysArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_months");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// totalMonthsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_years");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// totalYearsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_hours");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// dhwHourArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_days");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// dhwDaysArray = entry.getValue().split(",");

				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_months");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// dhwMonthsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_years");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					// dhwYearsArray = entry.getValue().split(",");
				}
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (error) {
				new getConsuptionData().execute();
			} else {
				if (toast != null) {
					toast.cancel();
				}
				toast = Toast.makeText(ConsuptionActivitySwipe.this,
						"Complete", Toast.LENGTH_SHORT);
				toast.show();
				viewPager.setAdapter(new ConsuptionViewPagerAdapter(
						getSupportFragmentManager()));

				rgp.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						int radioButtonID = group.getCheckedRadioButtonId();
						View radioButton = group.findViewById(radioButtonID);
						int idx = group.indexOfChild(radioButton);
						viewPager.setCurrentItem(idx);
					}
				});
			}
			super.onPostExecute(result);
		}
	}

	@Override
	public void onClick(View v) {
		if (v == llBack) {
			onBackPressed();
		}
	}
	@Override
	 protected void onResume() {
	  // TODO Auto-generated method stub
	  super.onResume();
	  wakeLock.acquire();
	 }

	 @Override
	 protected void onPause() {
	  // TODO Auto-generated method stub
	  super.onPause();
	  wakeLock.release();
	 }
}