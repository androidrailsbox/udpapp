package com.kat.udpapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.polidea.view.ZoomView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.udpapp.R;
import com.kat.udpapp.exception.ParseException;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class MainActivity extends Activity {

	private NBEControllerClient client;
	private InetAddress controllerAddress, controllerAddressTemp;
	private InetAddress myAddress;
	private String password;
	ArrayAdapter<String> controlBoxIP;
	ArrayAdapter<String> controlBox;
	boolean isAddressCorrect = false;
	String myIP = null;
	ImageView mStopImageView, mSignBolier;
	InetAddress foundAddress;

	String[] boiler;
	String[] boilerValues;
	String[] Weather;
	String[] weatherValues;
	String[] hopper;
	String[] HopperValues;
	String[] DHW;
	String[] DHWValues;

	HashMap<Integer, String> boilerData;
	HashMap<Integer, String> HopperData;
	HashMap<Integer, String> WeatherData;
	HashMap<Integer, String> DHWData;

	private static final String PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	String srt;
	boolean isStop = true, isCounter = false, isInternet = true;
	private ZoomView zoomView;
	Button btnNotConnected;
	LinearLayout main_container;
	LinearLayout llOutput4, llOutput7, llOutput9, llOutput1, llOutput2,
			llOutput3, llOutput5, llOutput6, llOutput8, llOutput10;
	TextView mWeatherHeaderTextView, mLoginTextView, mMailTextView,
			txtSubstate, txtSubstateCounter, mBoilerTextView, mHooperTextView,
			mDhwTextView, mWeatherTextView,
			mSunControlTextView,
			mConsumptionTextView,
			mChartsTextView,
			mDownloadTextView,
			mLogTextView,
			mSystemTextView,
			mScreenTextView,
			mManualTextView,
			mStoppedTempTextView,
			mOutput1TextView,
			mOutput2TextView,
			mOutput3TextView,
			mOutput4TextView,
			mOutput5TextView,
			mOutput6TextView,
			mOutput7TextView,
			mOutput8TextView,
			mOutput9TextView,
			mOutput10TextView,
			// Weather lavel texts
			wLabel1TextView,
			wLabel2TextView,
			wLabel3TextView,
			wLabel4TextView,
			wLabel5TextView,
			sLabel1TextView,
			sLabel2TextView,
			sLabel3TextView,
			sLabel4TextView,
			sLabel5TextView,
			// Left label texts
			lLabel1TextView,
			lLabel2TextView,
			lLabel3TextView,
			lLabel4TextView,
			lLabel5TextView,
			lsLabel1TextView,
			lsLabel2TextView,
			lsLabel3TextView,
			lsLabel4TextView,
			lsLabel5TextView,
			// Mid label texts
			mLabel1TextView, mLabel2TextView, mLabel3TextView, mLabel4TextView,
			mLabel5TextView, mLabel6TextView, mLabel7TextView, mLabel8TextView,
			msLabel1TextView, msLabel2TextView, msLabel3TextView,
			msLabel4TextView,
			msLabel5TextView,
			msLabel6TextView,
			msLabel7TextView,
			msLabel8TextView,
			// Right label texts
			rLabel1TextView, rLabel2TextView, rLabel3TextView, rLabel4TextView,
			rLabel5TextView, rLabel6TextView, rLabel7TextView, rLabel8TextView,
			rsLabel1TextView, rsLabel2TextView, rsLabel3TextView,
			rsLabel4TextView, rsLabel5TextView, rsLabel6TextView,
			rsLabel7TextView, rsLabel8TextView,
			// Center text on right
			cLabel1TextView, cLabel2TextView;

	ProgressDialog progressDialog = null;

	ImageView img_login, img_mail, img_boiler, img_hooper, img_dhw,
			img_weather, img_suncontrol, img_compustion, img_charts,
			img_download, img_log, img_system, img_screen, img_manual;

	LinearLayout llBoiler1, llBoiler2, llBoiler3, llBoiler4, llBoiler5;
	LinearLayout llHopper1, llHopper2, llHopper3, llHopper4, llHopper5,
			llHopper6, llHopper7, llHopper8;
	LinearLayout llDHW1, llDHW2, llDHW3, llDHW4, llDHW5, llDHW6, llDHW7,
			llDHW8;
	LinearLayout llWeather1, llWeather2, llWeather3, llWeather4, llWeather5;

	Spinner spinner;

	// LinearLayout llEnglish, llDK;

	ArrayList<String> lists;

	Context context;
	String lang, edtString;
	Typeface myTypeface;

	String output_ash = "", output_burner = "", output_boiler1 = "",
			output_boiler2 = "", pump_output = "", weather_active = "",
			exhaust_fan = "";

	String subStateValue = "";
	View v;

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		setContentView(R.layout.activity_second);
		v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.mainview, null, false);
		v.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		context = this;
		zoomView = new ZoomView(this);
		zoomView.addView(v);

		myTypeface = Typeface.createFromAsset(getAssets(),
				"OPENSANS-REGULAR.TTF");

		new ExtracingLanguage().execute();
	}

	public class settingViews extends android.os.AsyncTask<Void, Void, Void>
			implements OnClickListener {

		List<ControllerResponse> list;

		@Override
		protected Void doInBackground(Void... params) {
			spinner = (Spinner) v.findViewById(R.id.spnLang);
			mStopImageView = (ImageView) v.findViewById(R.id.stop);
			mSignBolier = (ImageView) v.findViewById(R.id.boiler_sign);
			mWeatherHeaderTextView = (TextView) v
					.findViewById(R.id.weather_header);
			mLoginTextView = (TextView) v.findViewById(R.id.txt_login);
			mMailTextView = (TextView) v.findViewById(R.id.txt_mail);
			mBoilerTextView = (TextView) v.findViewById(R.id.txt_boiler);
			mHooperTextView = (TextView) v.findViewById(R.id.txt_hooper);
			mDhwTextView = (TextView) v.findViewById(R.id.txt_dhw);
			mWeatherTextView = (TextView) v.findViewById(R.id.txt_weather);
			btnNotConnected = (Button) v.findViewById(R.id.btnNotConnected);
			btnNotConnected.setVisibility(View.GONE);
			btnNotConnected.setOnClickListener(this);
			mSunControlTextView = (TextView) v
					.findViewById(R.id.txt_suncontrol);
			mConsumptionTextView = (TextView) v
					.findViewById(R.id.txt_compustion);
			mChartsTextView = (TextView) v.findViewById(R.id.txt_charts);
			mDownloadTextView = (TextView) v.findViewById(R.id.txt_download);
			mLogTextView = (TextView) v.findViewById(R.id.txt_log);
			mSystemTextView = (TextView) v.findViewById(R.id.txt_system);
			mScreenTextView = (TextView) v.findViewById(R.id.txt_screen);
			mManualTextView = (TextView) v.findViewById(R.id.txt_manual);
			mStoppedTempTextView = (TextView) v
					.findViewById(R.id.stopped_reached_temp);
			mOutput1TextView = (TextView) v.findViewById(R.id.output1);
			mOutput2TextView = (TextView) v.findViewById(R.id.output2);
			mOutput3TextView = (TextView) v.findViewById(R.id.output3);
			mOutput4TextView = (TextView) v.findViewById(R.id.output4);
			mOutput5TextView = (TextView) v.findViewById(R.id.output5);
			mOutput6TextView = (TextView) v.findViewById(R.id.output6);
			mOutput7TextView = (TextView) v.findViewById(R.id.output7);
			mOutput8TextView = (TextView) v.findViewById(R.id.output8);
			mOutput9TextView = (TextView) v.findViewById(R.id.output9);
			mOutput10TextView = (TextView) v.findViewById(R.id.output10);
			wLabel1TextView = (TextView) v.findViewById(R.id.weather_label1);
			wLabel2TextView = (TextView) v.findViewById(R.id.weather_label2);
			wLabel3TextView = (TextView) v.findViewById(R.id.weather_label3);
			wLabel4TextView = (TextView) v.findViewById(R.id.weather_label4);
			wLabel5TextView = (TextView) v.findViewById(R.id.weather_label5);
			sLabel1TextView = (TextView) v.findViewById(R.id.w_label1);
			sLabel2TextView = (TextView) v.findViewById(R.id.w_label2);
			sLabel3TextView = (TextView) v.findViewById(R.id.w_label3);
			sLabel4TextView = (TextView) v.findViewById(R.id.w_label4);
			sLabel5TextView = (TextView) v.findViewById(R.id.w_label5);
			txtSubstate = (TextView) v.findViewById(R.id.txtSubstate);
			txtSubstateCounter = (TextView) v
					.findViewById(R.id.txtSubstateCounter);

			sLabel1TextView.setSelected(true);
			sLabel2TextView.setSelected(true);
			sLabel3TextView.setSelected(true);
			sLabel4TextView.setSelected(true);
			sLabel5TextView.setSelected(true);

			// Left texts
			lLabel1TextView = (TextView) v.findViewById(R.id.left_label1);
			lLabel2TextView = (TextView) v.findViewById(R.id.left_label2);
			lLabel3TextView = (TextView) v.findViewById(R.id.left_label3);
			lLabel4TextView = (TextView) v.findViewById(R.id.left_label4);
			lLabel5TextView = (TextView) v.findViewById(R.id.left_label5);
			lsLabel1TextView = (TextView) v.findViewById(R.id.l_label1);
			lsLabel2TextView = (TextView) v.findViewById(R.id.l_label2);
			lsLabel3TextView = (TextView) v.findViewById(R.id.l_label3);
			lsLabel4TextView = (TextView) v.findViewById(R.id.l_label4);
			lsLabel5TextView = (TextView) v.findViewById(R.id.l_label5);

			lLabel1TextView.setSelected(true);
			lLabel2TextView.setSelected(true);
			lLabel3TextView.setSelected(true);
			lLabel4TextView.setSelected(true);
			lLabel4TextView.setSelected(true);
			// Mid texts
			mLabel1TextView = (TextView) v.findViewById(R.id.mid_label1);
			mLabel2TextView = (TextView) v.findViewById(R.id.mid_label2);
			mLabel3TextView = (TextView) v.findViewById(R.id.mid_label3);
			mLabel4TextView = (TextView) v.findViewById(R.id.mid_label4);
			mLabel5TextView = (TextView) v.findViewById(R.id.mid_label5);
			mLabel6TextView = (TextView) v.findViewById(R.id.mid_label6);
			mLabel7TextView = (TextView) v.findViewById(R.id.mid_label7);
			mLabel8TextView = (TextView) v.findViewById(R.id.mid_label8);
			msLabel1TextView = (TextView) v.findViewById(R.id.m_label1);
			msLabel2TextView = (TextView) v.findViewById(R.id.m_label2);
			msLabel3TextView = (TextView) v.findViewById(R.id.m_label3);
			msLabel4TextView = (TextView) v.findViewById(R.id.m_label4);
			msLabel5TextView = (TextView) v.findViewById(R.id.m_label5);
			msLabel6TextView = (TextView) v.findViewById(R.id.m_label6);
			msLabel7TextView = (TextView) v.findViewById(R.id.m_label7);
			msLabel8TextView = (TextView) v.findViewById(R.id.m_label8);

			mLabel1TextView.setSelected(true);
			mLabel2TextView.setSelected(true);
			mLabel3TextView.setSelected(true);
			mLabel4TextView.setSelected(true);
			mLabel5TextView.setSelected(true);
			mLabel6TextView.setSelected(true);
			mLabel7TextView.setSelected(true);
			mLabel8TextView.setSelected(true);

			// Right text
			rLabel1TextView = (TextView) v.findViewById(R.id.right_label1);
			rLabel2TextView = (TextView) v.findViewById(R.id.right_label2);
			rLabel3TextView = (TextView) v.findViewById(R.id.right_label3);
			rLabel4TextView = (TextView) v.findViewById(R.id.right_label4);
			rLabel5TextView = (TextView) v.findViewById(R.id.right_label5);
			rLabel6TextView = (TextView) v.findViewById(R.id.right_label6);
			rLabel7TextView = (TextView) v.findViewById(R.id.right_label7);
			rLabel8TextView = (TextView) v.findViewById(R.id.right_label8);
			rsLabel1TextView = (TextView) v.findViewById(R.id.r_label1);
			rsLabel2TextView = (TextView) v.findViewById(R.id.r_label2);
			rsLabel3TextView = (TextView) v.findViewById(R.id.r_label3);
			rsLabel4TextView = (TextView) v.findViewById(R.id.r_label4);
			rsLabel5TextView = (TextView) v.findViewById(R.id.r_label5);
			rsLabel6TextView = (TextView) v.findViewById(R.id.r_label6);
			rsLabel7TextView = (TextView) v.findViewById(R.id.r_label7);
			rsLabel8TextView = (TextView) v.findViewById(R.id.r_label8);
			cLabel1TextView = (TextView) v.findViewById(R.id.c_label1);
			cLabel2TextView = (TextView) v.findViewById(R.id.c_label2);

			rLabel1TextView.setSelected(true);
			rLabel2TextView.setSelected(true);
			rLabel3TextView.setSelected(true);
			rLabel4TextView.setSelected(true);
			rLabel5TextView.setSelected(true);
			rLabel6TextView.setSelected(true);
			rLabel7TextView.setSelected(true);
			rLabel8TextView.setSelected(true);

			llOutput1 = (LinearLayout) v.findViewById(R.id.llOutput1);
			llOutput2 = (LinearLayout) v.findViewById(R.id.llOutput2);
			llOutput3 = (LinearLayout) v.findViewById(R.id.llOutput3);
			llOutput4 = (LinearLayout) v.findViewById(R.id.llOutput4);
			llOutput5 = (LinearLayout) v.findViewById(R.id.llOutput5);
			llOutput6 = (LinearLayout) v.findViewById(R.id.llOutput6);
			llOutput7 = (LinearLayout) v.findViewById(R.id.llOutput7);
			llOutput8 = (LinearLayout) v.findViewById(R.id.llOutput8);
			llOutput9 = (LinearLayout) v.findViewById(R.id.llOutput9);
			llOutput10 = (LinearLayout) v.findViewById(R.id.llOutput10);

			// LinearLayouts
			llBoiler1 = (LinearLayout) v.findViewById(R.id.llBoiler1);
			llBoiler2 = (LinearLayout) v.findViewById(R.id.llBoiler2);
			llBoiler3 = (LinearLayout) v.findViewById(R.id.llBoiler3);
			llBoiler4 = (LinearLayout) v.findViewById(R.id.llBoiler4);
			llBoiler5 = (LinearLayout) v.findViewById(R.id.llBoiler5);

			llDHW1 = (LinearLayout) v.findViewById(R.id.llDHW1);
			llDHW2 = (LinearLayout) v.findViewById(R.id.llDHW2);
			llDHW3 = (LinearLayout) v.findViewById(R.id.llDHW3);
			llDHW4 = (LinearLayout) v.findViewById(R.id.llDHW4);
			llDHW5 = (LinearLayout) v.findViewById(R.id.llDHW5);
			llDHW6 = (LinearLayout) v.findViewById(R.id.llDHW6);
			llDHW7 = (LinearLayout) v.findViewById(R.id.llDHW7);
			llDHW8 = (LinearLayout) v.findViewById(R.id.llDHW8);

			llWeather1 = (LinearLayout) v.findViewById(R.id.llWeather1);
			llWeather2 = (LinearLayout) v.findViewById(R.id.llWeather2);
			llWeather3 = (LinearLayout) v.findViewById(R.id.llWeather3);
			llWeather4 = (LinearLayout) v.findViewById(R.id.llWeather4);
			llWeather5 = (LinearLayout) v.findViewById(R.id.llWeather5);

			llHopper1 = (LinearLayout) v.findViewById(R.id.llHopper1);
			llHopper2 = (LinearLayout) v.findViewById(R.id.llHopper2);
			llHopper3 = (LinearLayout) v.findViewById(R.id.llHopper3);
			llHopper4 = (LinearLayout) v.findViewById(R.id.llHopper4);
			llHopper5 = (LinearLayout) v.findViewById(R.id.llHopper5);
			llHopper6 = (LinearLayout) v.findViewById(R.id.llHopper6);
			llHopper7 = (LinearLayout) v.findViewById(R.id.llHopper7);
			llHopper8 = (LinearLayout) v.findViewById(R.id.llHopper8);

			myTypeface = Typeface.createFromAsset(getAssets(),
					"OPENSANS-REGULAR.TTF");

			mWeatherHeaderTextView.setTypeface(myTypeface);
			mLoginTextView.setTypeface(myTypeface);
			mMailTextView.setTypeface(myTypeface);
			mBoilerTextView.setTypeface(myTypeface);
			mHooperTextView.setTypeface(myTypeface);
			mDhwTextView.setTypeface(myTypeface);
			mWeatherTextView.setTypeface(myTypeface);
			mSunControlTextView.setTypeface(myTypeface);
			mConsumptionTextView.setTypeface(myTypeface);
			mChartsTextView.setTypeface(myTypeface);
			mDownloadTextView.setTypeface(myTypeface);
			mLogTextView.setTypeface(myTypeface);
			mSystemTextView.setTypeface(myTypeface);
			mScreenTextView.setTypeface(myTypeface);
			mManualTextView.setTypeface(myTypeface);
			mOutput1TextView.setTypeface(myTypeface);
			mOutput2TextView.setTypeface(myTypeface);
			mOutput3TextView.setTypeface(myTypeface);
			mOutput4TextView.setTypeface(myTypeface);
			mOutput5TextView.setTypeface(myTypeface);
			mOutput6TextView.setTypeface(myTypeface);
			mOutput7TextView.setTypeface(myTypeface);
			mOutput8TextView.setTypeface(myTypeface);
			mOutput9TextView.setTypeface(myTypeface);
			mOutput10TextView.setTypeface(myTypeface);
			wLabel1TextView.setTypeface(myTypeface);
			wLabel2TextView.setTypeface(myTypeface);
			wLabel3TextView.setTypeface(myTypeface);
			wLabel4TextView.setTypeface(myTypeface);
			wLabel5TextView.setTypeface(myTypeface);
			sLabel1TextView.setTypeface(myTypeface);
			sLabel2TextView.setTypeface(myTypeface);
			sLabel3TextView.setTypeface(myTypeface);
			sLabel4TextView.setTypeface(myTypeface);
			sLabel5TextView.setTypeface(myTypeface);

			lLabel1TextView.setTypeface(myTypeface);
			lLabel2TextView.setTypeface(myTypeface);
			lLabel3TextView.setTypeface(myTypeface);
			lLabel4TextView.setTypeface(myTypeface);
			lLabel5TextView.setTypeface(myTypeface);
			lsLabel1TextView.setTypeface(myTypeface);
			lsLabel2TextView.setTypeface(myTypeface);
			lsLabel3TextView.setTypeface(myTypeface);
			lsLabel4TextView.setTypeface(myTypeface);
			lsLabel5TextView.setTypeface(myTypeface);
			// Mid label texts
			mLabel1TextView.setTypeface(myTypeface);
			mLabel2TextView.setTypeface(myTypeface);
			mLabel3TextView.setTypeface(myTypeface);
			mLabel4TextView.setTypeface(myTypeface);
			mLabel5TextView.setTypeface(myTypeface);
			mLabel6TextView.setTypeface(myTypeface);
			mLabel7TextView.setTypeface(myTypeface);
			mLabel8TextView.setTypeface(myTypeface);
			msLabel1TextView.setTypeface(myTypeface);
			msLabel2TextView.setTypeface(myTypeface);
			msLabel3TextView.setTypeface(myTypeface);
			msLabel4TextView.setTypeface(myTypeface);
			msLabel5TextView.setTypeface(myTypeface);
			msLabel6TextView.setTypeface(myTypeface);
			msLabel7TextView.setTypeface(myTypeface);
			msLabel8TextView.setTypeface(myTypeface);
			// Right label texts
			rLabel1TextView.setTypeface(myTypeface);
			rLabel2TextView.setTypeface(myTypeface);
			rLabel3TextView.setTypeface(myTypeface);
			rLabel4TextView.setTypeface(myTypeface);
			rLabel5TextView.setTypeface(myTypeface);
			rLabel6TextView.setTypeface(myTypeface);
			rLabel7TextView.setTypeface(myTypeface);
			rLabel8TextView.setTypeface(myTypeface);
			rsLabel1TextView.setTypeface(myTypeface);
			rsLabel2TextView.setTypeface(myTypeface);
			rsLabel3TextView.setTypeface(myTypeface);
			rsLabel4TextView.setTypeface(myTypeface);
			rsLabel5TextView.setTypeface(myTypeface);
			rsLabel6TextView.setTypeface(myTypeface);
			rsLabel7TextView.setTypeface(myTypeface);
			rsLabel8TextView.setTypeface(myTypeface);

			cLabel1TextView.setTypeface(myTypeface);
			cLabel2TextView.setTypeface(myTypeface);

			img_login = (ImageView) v.findViewById(R.id.img_login);
			img_mail = (ImageView) v.findViewById(R.id.img_mail);
			img_boiler = (ImageView) v.findViewById(R.id.img_boiler);
			img_hooper = (ImageView) v.findViewById(R.id.img_hooper);
			img_dhw = (ImageView) v.findViewById(R.id.img_dhw);
			img_weather = (ImageView) v.findViewById(R.id.img_weather);
			img_suncontrol = (ImageView) v.findViewById(R.id.img_suncontrol);
			img_compustion = (ImageView) v.findViewById(R.id.img_compustion);
			img_charts = (ImageView) v.findViewById(R.id.img_charts);
			img_download = (ImageView) v.findViewById(R.id.img_download);
			img_log = (ImageView) v.findViewById(R.id.img_log);
			img_system = (ImageView) v.findViewById(R.id.img_system);
			img_screen = (ImageView) v.findViewById(R.id.img_screen);
			img_manual = (ImageView) v.findViewById(R.id.img_manual);
			img_login.setOnClickListener(this);
			img_mail.setOnClickListener(this);
			img_boiler.setOnClickListener(this);
			img_hooper.setOnClickListener(this);
			img_dhw.setOnClickListener(this);
			img_weather.setOnClickListener(this);
			img_suncontrol.setOnClickListener(this);
			img_compustion.setOnClickListener(this);
			img_charts.setOnClickListener(this);
			img_download.setOnClickListener(this);
			img_log.setOnClickListener(this);
			img_system.setOnClickListener(this);
			img_screen.setOnClickListener(this);
			img_manual.setOnClickListener(this);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				main_container = (LinearLayout) findViewById(R.id.main_container);
				main_container.addView(zoomView);
			} catch (Exception e) {
				e.printStackTrace();
			}
			isAddressCorrect = false;

			if (Constants.language.equals(Constants.languageNameDK)) {
				spinner.setSelection(2);
			} else {
				spinner.setSelection(1);
			}

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			controlBoxIP = new ArrayAdapter<String>(MainActivity.this,
					android.R.layout.select_dialog_singlechoice);
			controlBox = new ArrayAdapter<String>(MainActivity.this,
					android.R.layout.select_dialog_singlechoice);

			if (getLocalIpAddress() != null) {
				Log.e("IP ADDRESS====", "" + getLocalIpAddress());
				myIP = getLocalIpAddress();

			} else {
				ALertInternetNotAvailable();
				btnNotConnected.setVisibility(View.VISIBLE);
				isInternet = false;
			}

			if (isInternet) {
				try {
					getDiscovery(InetAddress.getByName(myIP));
					System.out.println(controlBoxIP);
					if (controlBoxIP.isEmpty()) {
						controlBoxIP.add("(2303) 185.17.218.11");
						controlBox.add("185.17.218.11");
					}
					if (!controlBoxIP.isEmpty()) {
						Log.e("controlBoxIP====", "" + controlBoxIP.getItem(0));
						AlertDiscoverIPAddressMoreFound(controlBoxIP);
					} else {
						AlertForAddressInput();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// new GetDiscovery().execute();

			spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					String item = (String) parent.getItemAtPosition(position);
					if (item.equals("English")) {
						Constants.language = Constants.languageNameEnglise;
						SecurePreferences.savePreferences(context,
								Constants.prefLanguage,
								Constants.languageNameEnglise);
						changeLanguage();
					} else if (item.equals("DK")) {
						Constants.language = Constants.languageNameDK;
						SecurePreferences.savePreferences(context,
								Constants.prefLanguage,
								Constants.languageNameDK);
						changeLanguage();
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {

				}

			});
			if (lang.equals("")) {
				SecurePreferences.savePreferences(context,
						Constants.prefLanguage, Constants.languageNameEnglise);
				changeLanguage();
			}
			hidingViews();

		}

		@Override
		public void onClick(View v) {

			if (v == btnNotConnected) {
				if (getLocalIpAddress() != null) {
					Log.e("IP ADDRESS====", "" + getLocalIpAddress());
					myIP = getLocalIpAddress();
					isInternet = true;
				} else {
					ALertInternetNotAvailable();
					btnNotConnected.setVisibility(View.VISIBLE);
					isInternet = false;
				}
				if (isInternet) {
					if (controlBoxIP.isEmpty()) {
						controlBoxIP.add("(2303) 185.17.218.11");
						controlBox.add("185.17.218.11");
						AlertDiscoverIPAddressMoreFound(controlBoxIP);
					} else {
						AlertDiscoverIPAddressMoreFound(controlBoxIP);
					}
				}
			}
			if (v.getId() == R.id.img_login) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				// Toast.makeText(MainActivity.this, "Not Implemented",
				// Toast.LENGTH_SHORT).show();
				if (isInternet) {
					controlBoxIP.clear();
					controlBox.clear();
					try {
						getDiscovery(InetAddress.getByName(myIP));
						System.out.println(controlBoxIP);
						if (controlBoxIP.isEmpty()) {
							controlBoxIP.add("(2303) 185.17.218.11");
							controlBox.add("185.17.218.11");
						}
						if (!controlBoxIP.isEmpty()) {
							Log.e("controlBoxIP====",
									"" + controlBoxIP.getItem(0));
							AlertDiscoverIPAddressMoreFound(controlBoxIP);
						} else {
							AlertForAddressInput();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			if (v.getId() == R.id.img_mail) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_boiler) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_hooper) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_dhw) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_weather) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_suncontrol) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_compustion) {
				Intent intent = new Intent(MainActivity.this,
						ConsuptionActivitySwipe.class);
				startActivity(intent);
			}
			if (v.getId() == R.id.img_charts) {
				 Intent intent = new Intent(MainActivity.this,
				LoginActivity.class);
				 startActivity(intent);
//				Toast.makeText(MainActivity.this, "Not Implemented",
//						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_download) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_log) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_system) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
			if (v.getId() == R.id.img_screen) {
				Intent intent = new Intent(MainActivity.this,
						SettingsActivity.class);
				startActivityForResult(intent, 111);
			}
			if (v.getId() == R.id.img_manual) {
				// Intent intent = new Intent(MainActivity.this,
				// LoginActivity.class);
				// startActivity(intent);
				Toast.makeText(MainActivity.this, "Not Implemented",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public class GetDiscovery extends android.os.AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			super.onPostExecute(result);
		}

	}

	private void hidingViews() {
		if (SecurePreferences.getIntPreference(context, "kedel1") == 0) {
			llBoiler1.setVisibility(View.GONE);
		} else {
			llBoiler1.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "kedel2") == 0) {
			llBoiler2.setVisibility(View.GONE);
		} else {
			llBoiler2.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "kedel3") == 0) {
			llBoiler3.setVisibility(View.GONE);
		} else {
			llBoiler3.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "kedel4") == 0) {
			llBoiler4.setVisibility(View.GONE);
		} else {
			llBoiler4.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "kedel5") == 0) {
			llBoiler5.setVisibility(View.GONE);
		} else {
			llBoiler5.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb1") == 0) {
			llDHW1.setVisibility(View.INVISIBLE);
		} else {
			llDHW1.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb2") == 0) {
			llDHW2.setVisibility(View.INVISIBLE);
		} else {
			llDHW2.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb3") == 0) {
			llDHW3.setVisibility(View.INVISIBLE);
		} else {
			llDHW3.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb4") == 0) {
			llDHW4.setVisibility(View.INVISIBLE);
		} else {
			llDHW4.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb5") == 0) {
			llDHW5.setVisibility(View.INVISIBLE);
		} else {
			llDHW5.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb6") == 0) {
			llDHW6.setVisibility(View.INVISIBLE);
		} else {
			llDHW6.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb7") == 0) {
			llDHW7.setVisibility(View.INVISIBLE);
		} else {
			llDHW7.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vvb8") == 0) {
			llDHW8.setVisibility(View.INVISIBLE);
		} else {
			llDHW8.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo1") == 0) {
			llHopper1.setVisibility(View.GONE);
		} else {
			llHopper1.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo2") == 0) {
			llHopper2.setVisibility(View.GONE);
		} else {
			llHopper2.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo3") == 0) {
			llHopper3.setVisibility(View.GONE);
		} else {
			llHopper3.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo4") == 0) {
			llHopper4.setVisibility(View.GONE);
		} else {
			llHopper4.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo5") == 0) {
			llHopper5.setVisibility(View.GONE);
		} else {
			llHopper5.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo6") == 0) {
			llHopper6.setVisibility(View.GONE);
		} else {
			llHopper6.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo7") == 0) {
			llHopper7.setVisibility(View.GONE);
		} else {
			llHopper7.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "silo8") == 0) {
			llHopper8.setVisibility(View.GONE);
		} else {
			llHopper8.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vejr1") == 0) {
			llWeather1.setVisibility(View.GONE);
		} else {
			llWeather1.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vejr2") == 0) {
			llWeather2.setVisibility(View.GONE);
		} else {
			llWeather2.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vejr3") == 0) {
			llWeather3.setVisibility(View.GONE);
		} else {
			llWeather3.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vejr4") == 0) {
			llWeather4.setVisibility(View.GONE);
		} else {
			llWeather4.setVisibility(View.VISIBLE);
		}
		if (SecurePreferences.getIntPreference(context, "vejr5") == 0) {
			llWeather5.setVisibility(View.GONE);
		} else {
			llWeather5.setVisibility(View.VISIBLE);
		}
	}

	public class ExtracingLanguage extends
			android.os.AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			lists = new ArrayList<String>();

			try {

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(getResources().openRawResource(
								R.raw.lang_uk_final), "utf-8"));
				String lines = "";
				while ((lines = reader.readLine()) != null) {
					lists.add(lines);
				}
				SecurePreferences.savePreferences(
						Constants.languageNameEnglise, context, lists);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			lists = new ArrayList<String>();
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(getResources().openRawResource(
								R.raw.lang_dk_final), "utf-8"));
				String lines = "";
				try {
					while ((lines = reader.readLine()) != null) {
						lists.add(lines); //
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				SecurePreferences.savePreferences(Constants.languageNameDK,
						context, lists);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			lang = SecurePreferences.getStringPreference(context,
					Constants.prefLanguage);

			new settingViews().execute();

			boiler = new String[23];
			for (int i = 0; i < boiler.length; i++) {
				boiler[i] = SecurePreferences.getStringPreference(
						Constants.language, MainActivity.this, "lng_boil_" + i);
			}
			Constants.boiler = boiler;

			DHW = new String[13];
			for (int i = 0; i < DHW.length; i++) {
				DHW[i] = SecurePreferences.getStringPreference(
						Constants.language, MainActivity.this, "lng_dhw_" + i);
			}
			Constants.DHW = DHW;

			Weather = new String[10];
			for (int i = 0; i < Weather.length; i++) {
				Weather[i] = SecurePreferences.getStringPreference(
						Constants.language, MainActivity.this, "lng_weather_"
								+ i);
			}
			Constants.weather = Weather;

			hopper = new String[19];
			for (int i = 0; i < hopper.length; i++) {
				hopper[i] = SecurePreferences.getStringPreference(
						Constants.language, MainActivity.this, "lng_hopper_"
								+ i);
			}
			Constants.Hopper = hopper;
		}
	}

	private void AlertDiscoverIPAddressMoreFound(
			final ArrayAdapter<String> arrayAdap) {

		AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle("IP Address found");
		builderSingle.setCancelable(false);

		builderSingle.setNegativeButton("cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (Constants.controller.equals("")) {
							btnNotConnected.setVisibility(View.VISIBLE);
						}
					}
				});

		builderSingle.setAdapter(arrayAdap,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, final int which) {

						AlertDialog.Builder alert = new AlertDialog.Builder(
								MainActivity.this);
						alert.setTitle("Alert Dialog"); // Set Alert dialog
														// title here
						alert.setMessage("Enter valid Password"); // Message
																	// here
						alert.setCancelable(false);
						final EditText input = new EditText(MainActivity.this);
						input.setHint("Enter Password of control box");
						input.setText(SecurePreferences.getStringPreference(
								context, "conPassword"));

						alert.setView(input);

						alert.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {

										try {

											edtString = input.getEditableText()
													.toString();
											if (!edtString.equals("")) {

												SecurePreferences
														.savePreferences(
																context,
																"conPassword",
																edtString);

												final String strName = controlBox
														.getItem(which);
												AlertDialog.Builder builderInner = new AlertDialog.Builder(
														MainActivity.this);
												builderInner
														.setMessage(strName);
												builderInner
														.setCancelable(false);
												builderInner
														.setTitle("Your Selected Item is");
												builderInner
														.setPositiveButton(
																"Ok",
																new DialogInterface.OnClickListener() {

																	@Override
																	public void onClick(
																			DialogInterface dialog,
																			int which) {
																		edtString = strName
																				.toString()
																				.trim();
																		dialog.dismiss();
																		Constants.controller = edtString;
																		Constants.password = input
																				.getEditableText()
																				.toString();
																		new validatePassword()
																				.execute();

																	}
																});
												builderInner.show();
											} else {
												Toast.makeText(context,
														"Invalid Password",
														Toast.LENGTH_LONG)
														.show();
												if (Constants.controller
														.equals("")) {
													btnNotConnected
															.setVisibility(View.VISIBLE);
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}

									}
								});
						alert.setNegativeButton("CANCEL",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// Canceled.
										dialog.cancel();
										if (Constants.controller.equals("")) {
											btnNotConnected
													.setVisibility(View.VISIBLE);
										}
									}
								}); // End of alert.setNegativeButton

						AlertDialog alertDialog = alert.create();
						alertDialog.show();

					}
				});
		builderSingle.show();

	}

	public class validatePassword extends
			android.os.AsyncTask<Void, Void, Void> {

		int statusCode = 0;

		@Override
		protected Void doInBackground(Void... params) {
			try {
				client = new NBEControllerClient();
				password = Constants.password;

				myAddress = InetAddress.getByName(myIP);
				controllerAddress = InetAddress.getByName(Constants.controller
						.toString().replace("/", ""));
				ControllerRequest cr = new ControllerRequestImpl();
				cr.setReadRequest(password, "boiler.*");
				ControllerResponse res2 = client.sendRequest(myAddress,
						controllerAddress, cr);
				statusCode = res2.getStatusCode();
			} catch (Exception e) {
				e.printStackTrace();
				statusCode = 1;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (statusCode == 1) {
				Toast.makeText(MainActivity.this,
						"Invalid Password! Try Again", Toast.LENGTH_LONG)
						.show();
				btnNotConnected.setVisibility(View.VISIBLE);
			} else {
				Toast.makeText(MainActivity.this, "Valid Password",
						Toast.LENGTH_LONG).show();

				new AsyncTask().execute(edtString);
				btnNotConnected.setVisibility(View.GONE);
			}
		}
	}

	private class AsyncTask extends
			android.os.AsyncTask<String, String, String> {

		String strAdd = "0";
		boolean error;

		@SuppressLint("UseSparseArrays")
		@Override
		protected String doInBackground(final String... params) {

			try {

				boilerData = new HashMap<Integer, String>();
				WeatherData = new HashMap<Integer, String>();
				HopperData = new HashMap<Integer, String>();
				DHWData = new HashMap<Integer, String>();

				Log.e("TIME(in 5 sec)======", "print");

				client = new NBEControllerClient();
				password = Constants.password;
				myAddress = InetAddress
						.getByName(myIP/* ip *//* "192.168.1.124" */);
				controllerAddress = InetAddress.getByName(Constants.controller
						.toString().replace("/", ""));
				ControllerRequest cr = new ControllerRequestImpl();
				cr.setReadRequest(password, "boiler.*"); // Get a list of all
															// boiler values
				ControllerResponse res2 = client.sendRequest(myAddress,
						controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				Map<String, String> map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString().equalsIgnoreCase("temp")) {
						strAdd = entry.getValue()/* +"�C" */;
					}
					if (entry.getKey().toString().equalsIgnoreCase("diff_over")) {
						boilerData.put(21, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("diff_under")) {
						boilerData.put(22, entry.getValue() + "�C");
					}
				}

				cr.setReadRequest(password, "hot_water.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString()
							.equalsIgnoreCase("diff_under")) {
						DHWData.put(3, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("output")) {
						DHWData.put(6, "L"+entry.getValue());
					}
				}

				cr.setReadRequest(password, "regulation.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_gain_p")) {
						boilerData.put(18, entry.getValue());
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_gain_i")) {
						boilerData.put(19, entry.getValue());
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_power_min")) {
						HopperData.put(7, entry.getValue());
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_power_max")) {
						HopperData.put(8, entry.getValue());
					}
				}

				cr.setReadRequest(password, "hopper.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString().equalsIgnoreCase("content")) {
						HopperData.put(1, entry.getValue() + "kg");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("auger_capacity")) {
						HopperData.put(2, entry.getValue() + " g/6 min");
					}
					if (entry.getKey().toString().equalsIgnoreCase("trip1")) {
						HopperData.put(5, entry.getValue() + "kg");
					}
					if (entry.getKey().toString().equalsIgnoreCase("trip2")) {
						HopperData.put(13, entry.getValue() + "%");
					}
				}

				cr.setReadRequest(password, "cleaning.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString()
							.equalsIgnoreCase("output_ash")) {
						output_ash = "L"+entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("output_burner")) {
						output_burner ="L"+entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("output_boiler1")) {
						output_boiler1 ="L"+entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("output_boiler2")) {
						output_boiler2 ="L"+entry.getValue();
					}
				}

				cr.setReadRequest(password, "pump.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString().equalsIgnoreCase("output")) {
						pump_output = "L"+entry.getValue();
					}
				}
				cr.setReadRequest(password, "weather.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString().equalsIgnoreCase("active")) {
						weather_active = entry.getValue();
					}
				}
				cr.setReadRequest(password, "fan.*");
				res2 = client.sendRequest(myAddress, controllerAddress, cr);
				Log.e("ControllerResponse====", res2.toString());

				map = res2.getReadValues();
				for (Entry<String, String> entry : map.entrySet()) {
					if (entry.getKey().toString()
							.equalsIgnoreCase("exhaust_fan")) {
						exhaust_fan = "L"+entry.getValue();
					}
				}
			} catch (IOException e) {
				error = true;
				e.printStackTrace();
			} catch (ParseException e) {
				error = true;
				e.printStackTrace();
			} catch (Exception e) {
				error = true;
				e.printStackTrace();
			}
			return strAdd;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.e("ON POST EXECUTE======", "" + strAdd.toString());

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					new BoilerData().execute();
				}
			}, 1000);

			if (!error) {
				// wLabel1TextView.setText(result/* +"�C" */);
			}
		}
	}

	public class BoilerData extends android.os.AsyncTask<Void, Void, Void> {

		String city = "", state = "", subState = "", sub_state_sec = "",
				ash_clean = "", compressor_clean = "", boiler_pump_state = "",
				house_valve_state = "", house_pump_state = "",
				exhaust_speed = "";

		String dhw_valve_state = "";

		boolean error = false;

		@Override
		protected Void doInBackground(Void... params) {
			try {
				System.out.println("Begin Boiler");

				password = Constants.password;

				myAddress = InetAddress.getByName(myIP);

				ControllerRequest cr = new ControllerRequestImpl();
				cr.setOperationRequest(password, "*"); // Get a single drift
														// values

				controllerAddress = InetAddress.getByName(Constants.controller);

				ControllerResponse res2 = client.sendRequest(myAddress,
						controllerAddress, cr);
				System.out.println(res2.toString());
				Map<String, String> map = res2.getOperationValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " - "
							+ entry.getValue());

					// Key is interger index of arrary from resource.
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_temp")) {
						boilerData.put(1, entry.getValue() + "�C");

					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_ref")) {
						boilerData.put(2, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("smoke_temp")) {
						boilerData.put(3, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("power_pct")) {
						boilerData.put(4, entry.getValue() + "%");
					}
					if (entry.getKey().toString().equalsIgnoreCase("power_kw")) {
						boilerData.put(5, entry.getValue() + "kw");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("photo_level")) {
						boilerData.put(6, entry.getValue() + "lux");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("shaft_temp")) {
						boilerData.put(7, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("flow1")) {
						boilerData.put(10, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("oxygen")) {
						boilerData.put(12, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("oxygen_ref")) {
						boilerData.put(13, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("return_temp")) {
						boilerData.put(17, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("distance")) {
						HopperData.put(14, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("feed_medium")) {
						HopperData.put(19, entry.getValue() + "%");
					}
					if (entry.getKey().toString().equalsIgnoreCase("feed_low")) {
						HopperData.put(9, entry.getValue() + "%");
					}
					if (entry.getKey().toString().equalsIgnoreCase("feed_high")) {
						HopperData.put(10, entry.getValue() + "%");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("outdoor_temp")) {
						WeatherData.put(1, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("wind_speed")) {
						WeatherData.put(2, entry.getValue() + "m/s");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("wind_direction")) {
						WeatherData.put(3, entry.getValue() + "");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("humidity")) {
						WeatherData.put(4, entry.getValue() + "%");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("air_pressure")) {
						WeatherData.put(5, entry.getValue() + "hPa");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("mean_out_temp")) {
						WeatherData.put(6, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("outdoor_temp")) { // Is this t5?
						WeatherData.put(7, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("chill_out")) {
						WeatherData.put(8, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("clouds")) {
						WeatherData.put(9, entry.getValue() + "%");
					}
					if (entry.getKey().toString().equalsIgnoreCase("dhw_temp")) {
						DHWData.put(1, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("dhw_ref")) {
						DHWData.put(2, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("sun_temp")) {
						DHWData.put(7, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("sun_dhw_temp")) {
						DHWData.put(8, entry.getValue() + "�C");
					}
					if (entry.getKey().toString().equalsIgnoreCase("sun2_temp")) {
						DHWData.put(9, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("sun_power_kw")) {
						DHWData.put(12, entry.getValue() + "�C");
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("dhw_valve_state")) {
						dhw_valve_state = entry.getValue();
					}

					if (entry.getKey().toString().equalsIgnoreCase("city")) {
						city = entry.getValue();
					}
					if (entry.getKey().toString().equalsIgnoreCase("state")) {
						state = entry.getValue();
					}
					if (entry.getKey().toString().equalsIgnoreCase("substate")) {
						subState = entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("substate_sec")) {
						sub_state_sec = entry.getValue();
					}
					if (entry.getKey().toString().equalsIgnoreCase("ash_clean")) {
						ash_clean = entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("compressor_clean")) {
						compressor_clean = entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("boiler_pump_state")) {
						boiler_pump_state = entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("house_valve_state")) {
						house_valve_state = entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("house_pump_state")) {
						house_pump_state = entry.getValue();
					}
					if (entry.getKey().toString()
							.equalsIgnoreCase("exhaust_speed")) {
						exhaust_speed = entry.getValue();
					}
				}
				System.out.println("End Boiler");
			} catch (Exception e) {
				error = true;
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (!error) {

				mWeatherHeaderTextView.setText(city);
				if (state.equals("0") || state.equals("1") || state.equals("2")
						|| state.equals("3") || state.equals("4")
						|| state.equals("5") || state.equals("6")
						|| state.equals("7") || state.equals("10")
						|| state.equals("18") || state.equals("21")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_fire2));

				} else if (state.equals("8") || state.equals("11")
						|| state.equals("12") || state.equals("13")
						|| state.equals("15") || state.equals("16")
						|| state.equals("17") || state.equals("19")
						|| state.equals("20") || state.equals("26")
						|| state.equals("27") || state.equals("28")
						|| state.equals("29")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_alarm2));
				} else if (state.equals("9")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_temp));
				} else if (state.equals("14")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_off));
				} else if (state.equals("22") || state.equals("25")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_weather));
				} else if (state.equals("23")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_clock));
				} else if (state.equals("24")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_contact));
				} else if (state.equals("30")) {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_stopped_cascade));
				} else {
					mSignBolier.setImageDrawable(getResources().getDrawable(
							R.drawable.sign_fire2));
				}

				if (DHWData.get(6) != null) {
					if (!DHWData.get(6).equals("L0")) {
						llOutput4.setVisibility(View.VISIBLE);
						if (dhw_valve_state.equals("0")) {
							mOutput4TextView.setText("OFF");
						} else {
							mOutput4TextView.setText("ON");
						}
					} else {
						llOutput4.setVisibility(View.INVISIBLE);
					}
				} else {
					llOutput4.setVisibility(View.INVISIBLE);
				}

				if (pump_output.equals("L0")) {
					llOutput1.setVisibility(View.INVISIBLE);
				} else {
					llOutput1.setVisibility(View.VISIBLE);
					if (boiler_pump_state.equals("0")) {
						mOutput1TextView.setText("OFF");
					} else {
						mOutput1TextView.setText("ON");
					}
				}

				if (weather_active.equals("0")) {
					llOutput2.setVisibility(View.GONE);
					llOutput3.setVisibility(View.GONE);
				} else {
					llOutput2.setVisibility(View.VISIBLE);
					llOutput3.setVisibility(View.VISIBLE);
					mOutput2TextView.setText(house_valve_state + "%");
					if (house_pump_state.equals("0")) {
						mOutput3TextView.setText("OFF");
					} else {
						mOutput3TextView.setText("ON");
					}
				}
				if (exhaust_fan.equals("L0")) {
					llOutput10.setVisibility(View.VISIBLE);
					mOutput10TextView.setText(exhaust_speed + "%");
				} else {
					llOutput10.setVisibility(View.INVISIBLE);
				}

				if (output_ash.equals("L0")) {
					llOutput7.setVisibility(View.INVISIBLE);
				} else {
					llOutput7.setVisibility(View.VISIBLE);
					mOutput7TextView.setText(ash_clean + " kg");
				}
				if (output_burner.equals("L0") || output_boiler1.equals("L0")
						|| output_boiler2.equals("L0")) {
					llOutput9.setVisibility(View.INVISIBLE);
				} else {
					llOutput9.setVisibility(View.VISIBLE);
					mOutput9TextView.setText(compressor_clean + " kg");
				}

				mStoppedTempTextView.setText(SecurePreferences
						.getStringPreference(Constants.language,
								MainActivity.this, "state_" + state));
				// subState = "5";
				if (!subState.trim().equals("0")) {
					if (!isCounter) {
						isCounter = true;
						txtSubstate.setVisibility(View.VISIBLE);
						txtSubstateCounter.setVisibility(View.VISIBLE);
						txtSubstate.setText(SecurePreferences
								.getStringPreference(Constants.language,
										MainActivity.this, "lng_substate_"
												+ subState));

						subStateValue = SecurePreferences.getStringPreference(
								Constants.language, MainActivity.this,
								"lng_substate_" + subState);

						// sub_state_sec = "500";
						if (!sub_state_sec.equals("0")) {
							long timer = Long.parseLong(sub_state_sec) * 1000;
							new CountDownTimer(timer, 1000) {// CountDownTimer(edittext1.getText()+edittext2.getText())
																// also parse it
																// to long

								public void onTick(long millisUntilFinished) {
									txtSubstate
											.setText(subStateValue
													+ " ( "
													+ (millisUntilFinished / 60000)
													+ " : "
													+ (millisUntilFinished % 60000 / 1000)
													+ " )");
								}

								public void onFinish() {
									txtSubstate.setText("over");
									txtSubstate.setVisibility(View.INVISIBLE);

									isCounter = false;
								}
							}.start();
						}
					}
				} else {
					txtSubstate.setVisibility(View.INVISIBLE);
				}

				displayData();

				// if (SecurePreferences.getIntPreference(context, "kedel1") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "kedel1");
				// lLabel1TextView.setText(boiler[position]);
				// if (boilerData.get(position) != null)
				// lsLabel1TextView.setText(boilerData.get(position));
				// else
				// lsLabel1TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "kedel2") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "kedel2");
				// lLabel2TextView.setText(boiler[position]);
				// if (boilerData.get(position) != null)
				// lsLabel2TextView.setText(boilerData.get(position));
				// else
				// lsLabel2TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "kedel3") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "kedel3");
				// lLabel3TextView.setText(boiler[position]);
				// if (boilerData.get(position) != null)
				// lsLabel3TextView.setText(boilerData.get(position));
				// else
				// lsLabel3TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "kedel4") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "kedel4");
				// lLabel4TextView.setText(boiler[position]);
				// if (boilerData.get(position) != null)
				// lsLabel4TextView.setText(boilerData.get(position));
				// else
				// lsLabel4TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "kedel5") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "kedel5");
				// lLabel5TextView.setText(boiler[position]);
				// if (boilerData.get(position) != null)
				// lsLabel5TextView.setText(boilerData.get(position));
				// else
				// lsLabel5TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo1") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo1");
				// mLabel1TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel1TextView.setText(HopperData.get(position));
				// else
				// msLabel1TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo2") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo2");
				// mLabel2TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel2TextView.setText(HopperData.get(position));
				// else
				// msLabel2TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo3") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo3");
				// mLabel3TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel3TextView.setText(HopperData.get(position));
				// else
				// msLabel3TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo4") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo4");
				// mLabel4TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel4TextView.setText(HopperData.get(position));
				// else
				// msLabel4TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo5") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo5");
				// mLabel5TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel5TextView.setText(HopperData.get(position));
				// else
				// msLabel5TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo6") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo6");
				// mLabel6TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel6TextView.setText(HopperData.get(position));
				// else
				// msLabel6TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo7") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo7");
				// mLabel7TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel7TextView.setText(HopperData.get(position));
				// else
				// msLabel7TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "silo8") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "silo8");
				// mLabel8TextView.setText(hopper[position]);
				// if (HopperData.get(position) != null)
				// msLabel8TextView.setText(HopperData.get(position));
				// else
				// msLabel8TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vejr1") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "vejr1");
				// sLabel1TextView.setText(Weather[position]);
				// if (WeatherData.get(position) != null)
				// wLabel1TextView.setText(WeatherData.get(position));
				// else
				// wLabel1TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vejr2") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "vejr2");
				// sLabel2TextView.setText(Weather[position]);
				// if (WeatherData.get(position) != null)
				// wLabel2TextView.setText(WeatherData.get(position));
				// else
				// wLabel2TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vejr3") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "vejr3");
				// sLabel3TextView.setText(Weather[position]);
				// if (WeatherData.get(position) != null)
				// wLabel3TextView.setText(WeatherData.get(position));
				// else
				// wLabel3TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vejr4") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "vejr4");
				// sLabel4TextView.setText(Weather[position]);
				// if (WeatherData.get(position) != null)
				// wLabel4TextView.setText(WeatherData.get(position));
				// else
				// wLabel4TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vejr5") !=
				// 0) {
				// int position = SecurePreferences.getIntPreference(context,
				// "vejr5");
				// sLabel5TextView.setText(Weather[position]);
				// if (WeatherData.get(position) != null)
				// wLabel5TextView.setText(WeatherData.get(position));
				// else
				// wLabel5TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb1") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb1");
				// rLabel1TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel1TextView.setText(DHWData.get(position));
				// else
				// rsLabel1TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb2") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb2");
				// rLabel2TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel2TextView.setText(DHWData.get(position));
				// else
				// rsLabel2TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb3") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb3");
				// rLabel3TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel3TextView.setText(DHWData.get(position));
				// else
				// rsLabel3TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb4") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb4");
				// rLabel4TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel4TextView.setText(DHWData.get(position));
				// else
				// rsLabel4TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb5") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb5");
				// rLabel5TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel5TextView.setText(DHWData.get(position));
				// else
				// rsLabel5TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb6") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb6");
				// rLabel6TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel6TextView.setText(DHWData.get(position));
				// else
				// rsLabel6TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb7") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb7");
				// rLabel7TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel7TextView.setText(DHWData.get(position));
				// else
				// rsLabel7TextView.setText("N/A");
				// }
				// if (SecurePreferences.getIntPreference(context, "vvb8") != 0)
				// {
				// int position = SecurePreferences.getIntPreference(context,
				// "vvb8");
				// rLabel8TextView.setText(DHW[position]);
				// if (DHWData.get(position) != null)
				// rsLabel8TextView.setText(DHWData.get(position));
				// else
				// rsLabel8TextView.setText("N/A");
				// }
			}

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					new AsyncTask().execute(edtString);
				}
			}, 5000);
		}
	}

	public void displayData() {
		if (SecurePreferences.getIntPreference(context, "kedel1") != 0) {
			int position = SecurePreferences
					.getIntPreference(context, "kedel1");
			lLabel1TextView.setText(boiler[position]);
			if (boilerData.get(position) != null)
				lsLabel1TextView.setText(boilerData.get(position));
			else
				lsLabel1TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "kedel2") != 0) {
			int position = SecurePreferences
					.getIntPreference(context, "kedel2");
			lLabel2TextView.setText(boiler[position]);
			if (boilerData.get(position) != null)
				lsLabel2TextView.setText(boilerData.get(position));
			else
				lsLabel2TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "kedel3") != 0) {
			int position = SecurePreferences
					.getIntPreference(context, "kedel3");
			lLabel3TextView.setText(boiler[position]);
			if (boilerData.get(position) != null)
				lsLabel3TextView.setText(boilerData.get(position));
			else
				lsLabel3TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "kedel4") != 0) {
			int position = SecurePreferences
					.getIntPreference(context, "kedel4");
			lLabel4TextView.setText(boiler[position]);
			if (boilerData.get(position) != null)
				lsLabel4TextView.setText(boilerData.get(position));
			else
				lsLabel4TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "kedel5") != 0) {
			int position = SecurePreferences
					.getIntPreference(context, "kedel5");
			lLabel5TextView.setText(boiler[position]);
			if (boilerData.get(position) != null)
				lsLabel5TextView.setText(boilerData.get(position));
			else
				lsLabel5TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo1") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo1");
			mLabel1TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel1TextView.setText(HopperData.get(position));
			else
				msLabel1TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo2") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo2");
			mLabel2TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel2TextView.setText(HopperData.get(position));
			else
				msLabel2TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo3") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo3");
			mLabel3TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel3TextView.setText(HopperData.get(position));
			else
				msLabel3TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo4") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo4");
			mLabel4TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel4TextView.setText(HopperData.get(position));
			else
				msLabel4TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo5") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo5");
			mLabel5TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel5TextView.setText(HopperData.get(position));
			else
				msLabel5TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo6") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo6");
			mLabel6TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel6TextView.setText(HopperData.get(position));
			else
				msLabel6TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo7") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo7");
			mLabel7TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel7TextView.setText(HopperData.get(position));
			else
				msLabel7TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "silo8") != 0) {
			int position = SecurePreferences.getIntPreference(context, "silo8");
			mLabel8TextView.setText(hopper[position]);
			if (HopperData.get(position) != null)
				msLabel8TextView.setText(HopperData.get(position));
			else
				msLabel8TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vejr1") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vejr1");
			sLabel1TextView.setText(Weather[position]);
			if (WeatherData.get(position) != null)
				wLabel1TextView.setText(WeatherData.get(position));
			else
				wLabel1TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vejr2") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vejr2");
			sLabel2TextView.setText(Weather[position]);
			if (WeatherData.get(position) != null)
				wLabel2TextView.setText(WeatherData.get(position));
			else
				wLabel2TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vejr3") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vejr3");
			sLabel3TextView.setText(Weather[position]);
			if (WeatherData.get(position) != null)
				wLabel3TextView.setText(WeatherData.get(position));
			else
				wLabel3TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vejr4") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vejr4");
			sLabel4TextView.setText(Weather[position]);
			if (WeatherData.get(position) != null)
				wLabel4TextView.setText(WeatherData.get(position));
			else
				wLabel4TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vejr5") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vejr5");
			sLabel5TextView.setText(Weather[position]);
			if (WeatherData.get(position) != null)
				wLabel5TextView.setText(WeatherData.get(position));
			else
				wLabel5TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb1") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb1");
			rLabel1TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel1TextView.setText(DHWData.get(position));
			else
				rsLabel1TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb2") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb2");
			rLabel2TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel2TextView.setText(DHWData.get(position));
			else
				rsLabel2TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb3") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb3");
			rLabel3TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel3TextView.setText(DHWData.get(position));
			else
				rsLabel3TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb4") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb4");
			rLabel4TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel4TextView.setText(DHWData.get(position));
			else
				rsLabel4TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb5") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb5");
			rLabel5TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel5TextView.setText(DHWData.get(position));
			else
				rsLabel5TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb6") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb6");
			rLabel6TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel6TextView.setText(DHWData.get(position));
			else
				rsLabel6TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb7") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb7");
			rLabel7TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel7TextView.setText(DHWData.get(position));
			else
				rsLabel7TextView.setText("N/A");
		}
		if (SecurePreferences.getIntPreference(context, "vvb8") != 0) {
			int position = SecurePreferences.getIntPreference(context, "vvb8");
			rLabel8TextView.setText(DHW[position]);
			if (DHWData.get(position) != null)
				rsLabel8TextView.setText(DHWData.get(position));
			else
				rsLabel8TextView.setText("N/A");
		}
	}

	public void getDriftAll() throws IOException, ParseException {
		ControllerRequest cr = new ControllerRequestImpl();
		cr.setOperationRequest(password, "*"); // Get a single drift values

		myAddress = InetAddress.getByName(myIP/* ip *//* "192.168.1.124" */);
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);
		System.out.println(res2.toString());
		Toast.makeText(context, res2.toString(), Toast.LENGTH_LONG).show();
		Map<String, String> map = res2.getOperationValues();
		for (Entry<String, String> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " - " + entry.getValue());
		}
	}

	// private void ALertDiscoverIPAddress(String foundAdd) {
	//
	// // Alert Dialog Code Start
	// AlertDialog.Builder alert = new AlertDialog.Builder(this);
	// alert.setTitle("IP Address"); // Set Alert dialog title here
	// alert.setMessage(foundAdd.replace("/", "") + " found"); // Message here
	//
	// alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int whichButton) {
	// // You will get as string input data in this variable.
	// // here we convert the input to a string and show in a toast.
	// dialog.cancel();
	// } // End of onClick(DialogInterface dialog, int whichButton)
	// });
	// AlertDialog alertDialog = alert.create();
	// alertDialog.show();
	// }

	private String AlertForAddressInput() {
		/* Alert Dialog Code Start */
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Alert Dialog"); // Set Alert dialog title here
		alert.setMessage("Enter valid IP address"); // Message here

		// final Handler handler = new Handler();

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		// input.setInputType(InputType.TYPE_CLASS_NUMBER |
		// InputType.TYPE_NUMBER_FLAG_DECIMAL);
		alert.setView(input);
		input.setText("");

		alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// You will get as string input data in this variable.
				// here we convert the input to a string and show in a toast.
				srt = input.getEditableText().toString();

				if (!validate(srt)) {
					Toast.makeText(MainActivity.this, "Invalid IP Address",
							Toast.LENGTH_LONG).show();
				} else {
					try {
						isAddressCorrect = true;
						controllerAddressTemp = InetAddress.getByName(srt);

						edtString = srt;

						new AsyncTask().execute(controllerAddressTemp
								.toString());
					} catch (UnknownHostException e) {
						e.printStackTrace();
					}
				}
			} // End of onClick(DialogInterface dialog, int whichButton)
		}); // End of alert.setPositiveButton
		alert.setNegativeButton("CANCEL",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
						dialog.cancel();
					}
				}); // End of alert.setNegativeButton

		AlertDialog alertDialog = alert.create();
		alertDialog.show();
		return null;

	}

	// Function to check for IP address enter is valid or not
	public static boolean validate(final String ip) {
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(ip);
		return matcher.matches();
	}

	private void getDiscovery(InetAddress add) {

		Log.e("MESSAGE====", "CLICK HERE calll here" + add);
		client = new NBEControllerClient();
		List<ControllerResponse> list;
		try {
			list = client.discoverControllers(add);
			Log.e("MESSAGE 1====", "CLICK HERE calll here");
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					ControllerResponse res = list.get(i);
					String values[] = res.getDiscoveryValues();
					String ip = values[0];
					String serial = values[1];
					controllerAddress = InetAddress.getByName(ip);
					myAddress = res.getSenderAddress();
					Log.e("MESSAGE====", "CLICK HERE");
					Log.e("Found controller with serialnumber", " " + serial
							+ " on ip address " + ip);
					controlBoxIP.add("(" + serial + ") " + ip);
					controlBox.add(ip);

				}
				controlBoxIP.add("(2303) 185.17.218.11");
				controlBox.add("185.17.218.11");

				// ControllerResponse res = list.get(0); // Here it takes only
				// first element, but list size might be larger than 1 we need
				// all
				// String values[] = res.getDiscoveryValues();
				// String ip = values[0];
				// String serial = values[1];
				// controllerAddress = InetAddress.getByName(ip);
				// myAddress = res.getSenderAddress();
				// Log.e("MESSAGE====", "CLICK HERE");
				// Log.e("Found controller with serialnumber", " " + serial
				// + " on ip address " + ip);
				// controlBoxIP.add(ip);
			} else {
				Log.e("MESSAGE===", "No controller found!");
				throw new IOException("No controller+");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	InetAddress getBroadcastAddress() throws IOException {
		WifiManager wifi = (WifiManager) this
				.getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow

		int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		Log.e("ADDRESS FOUND=====", InetAddress.getByAddress(quads).toString());
		return InetAddress.getByAddress(quads);
	}

	public String getLocalIpAddress() {
		try {
			String ip = null;
			ConnectivityManager connManager = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			WifiManager wifiMan = (WifiManager) this
					.getSystemService(Context.WIFI_SERVICE);
			if (wifiMan.isWifiEnabled()) {
				WifiInfo wifiInf = wifiMan.getConnectionInfo();

				int ipAddress = wifiInf.getIpAddress();
				ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
						(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
						(ipAddress >> 24 & 0xff));
				return ip;
			} else if ((netInfo != null) && netInfo.isConnected()) {

				for (Enumeration<NetworkInterface> en = NetworkInterface
						.getNetworkInterfaces(); en.hasMoreElements();) {
					NetworkInterface intf = en.nextElement();
					for (Enumeration<InetAddress> enumIpAddr = intf
							.getInetAddresses(); enumIpAddr.hasMoreElements();) {
						InetAddress inetAddress = enumIpAddr.nextElement();
						if (!inetAddress.isLoopbackAddress()) {
							ip = inetAddress.getHostAddress().toString();
						}
					}
				}
				return ip;
			}

		} catch (Exception ex) {
			Log.e("IP Address", ex.toString());
		}

		return null;
	}

	public void getBoilerAll(InetAddress addcontroller) throws IOException,
			ParseException {
		client = new NBEControllerClient();
		password = Constants.password;
		Log.e("myIP===========", "" + myIP);
		Log.e("controllerAddress====",
				"" + addcontroller.toString().replace("/", ""));
		myAddress = InetAddress.getByName(myIP);
		controllerAddress = InetAddress.getByName(addcontroller.toString()
				.replace("/", ""));

		ControllerRequest cr = new ControllerRequestImpl();
		cr.setReadRequest(password, "boiler.*"); // Get a list of all boiler
													// values
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);
		Log.e("ControllerResponse====", res2.toString());
		Map<String, String> map = res2.getReadValues();
		for (Entry<String, String> entry : map.entrySet()) {
			Log.e("Entry Key=====", entry.getKey() + " : " + entry.getValue());
			Log.e("SANNN Key====", "" + entry.getKey().toString());
			Log.e("SANNN KeyVALUE====", "" + entry.getValue());
			if (entry.getKey().toString().equalsIgnoreCase("temp")) {
				Log.e("SANNNNNNN======", entry.getValue() + "�C");
				wLabel1TextView.setText(entry.getValue() + "�C");
			}
		}

	}

	public void getBoilerTemp(InetAddress address) throws IOException,
			ParseException {
		client = new NBEControllerClient();
		password = Constants.password;
		myAddress = InetAddress.getByName(myIP);
		controllerAddress = address;

		ControllerRequest cr = new ControllerRequestImpl();
		cr.setReadRequest(password, "boiler.temp"); // Get the boiler
													// temperature
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);
		Log.e("getBoilerTemp RES", res2.toString());
		// mResultTextView.setText(res2.toString());
		Map<String, String> map = res2.getReadValues();
		for (Entry<String, String> entry : map.entrySet()) {
			Log.e("getBoilerTemp KEY",
					entry.getKey() + " : " + entry.getValue());
		}
	}

	public void getHotWaterTimer(InetAddress addr) throws IOException,
			ParseException {
		ControllerRequest cr = new ControllerRequestImpl();
		password = Constants.password;
		myAddress = InetAddress.getByName(myIP);
		controllerAddress = addr;
		Log.e("GET HOT WATER===", "" + password);
		cr.setReadRequest(password, "hot_water.timer_mon"); // Get all timers
															// for monday (12
															// total)
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);
		Log.e("getHotWaterTimer RES", res2.toString());
		// mResultTextView.setText(res2.toString());
		Map<String, String> map = res2.getReadValues();
		for (Entry<String, String> entry : map.entrySet()) {
			Log.e("getHotWaterTimer KEY",
					entry.getKey() + " : " + entry.getValue());
		}
	}

	public void setBoilerTemp(int value) throws IOException, ParseException {
		client = new NBEControllerClient();
		ControllerRequest cr = new ControllerRequestImpl();
		password = Constants.password;
		myAddress = InetAddress.getByName(myIP);
		controllerAddress = InetAddress.getByName(Constants.controller);
		cr.setSetRequest(password, "boiler.temp", "" + value); // Set the boiler
																// temperature
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);

		if (res2.getStatusCode() == 0) {
			Log.e("Boiler temperature set ok to ", "" + value);
			// mResultTextView.setText(""+value);
		} else {
			Log.e("Failed to set boiler temperature, error code ",
					"" + res2.getStatusCode());
		}
	}

	public void changeLanguage() {

		boiler = new String[23];
		for (int i = 0; i < boiler.length; i++) {
			boiler[i] = SecurePreferences.getStringPreference(
					Constants.language, MainActivity.this, "lng_boil_" + i);
		}
		Constants.boiler = boiler;

		DHW = new String[13];
		for (int i = 0; i < DHW.length; i++) {
			DHW[i] = SecurePreferences.getStringPreference(Constants.language,
					MainActivity.this, "lng_dhw_" + i);
		}
		Constants.DHW = DHW;

		Weather = new String[10];
		for (int i = 0; i < Weather.length; i++) {
			Weather[i] = SecurePreferences.getStringPreference(
					Constants.language, MainActivity.this, "lng_weather_" + i);
		}
		Constants.weather = Weather;

		hopper = new String[19];
		for (int i = 0; i < hopper.length; i++) {
			hopper[i] = SecurePreferences.getStringPreference(
					Constants.language, MainActivity.this, "lng_hopper_" + i);
		}
		Constants.Hopper = hopper;

		mLoginTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_login"));
		mMailTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_mail"));
		mBoilerTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_setup_screen_boiler_header"));
		mHooperTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_hopper"));
		mDhwTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_dhw"));
		mWeatherTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_weather"));
		mSunControlTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_sun"));
		mConsumptionTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_consumption"));
		mChartsTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_charts"));
		mDownloadTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_download"));
		mLogTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_log"));
		mSystemTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_system"));
		mScreenTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_screen"));
		mManualTextView.setText(SecurePreferences.getStringPreference(
				Constants.language, context, "lng_button_manual"));
	}

	// Internet connection check Alert
	private void ALertInternetNotAvailable() {
		AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
				.create();

		// Setting Dialog Title
		alertDialog.setTitle("Alert Dialog");

		// Setting Dialog Message
		alertDialog.setMessage("On network connection");

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_launcher);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// Write your code here to execute after dialog closed

			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		hidingViews();
		displayData();
	}

	@Override
	protected void onDestroy() {
		// Added by stokersoft to exit application
		this.finish();
		android.os.Process.killProcess(android.os.Process.myPid());
		super.onDestroy();
	}
	
	

}
