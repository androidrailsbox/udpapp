package com.kat.udpapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.achartengine.util.MathHelper;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.Toast;

import com.example.udpapp.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;
import com.kat.udpapp.chart.MyValueFormatter;

public class ConsuptionViewPagerAdapter extends FragmentPagerAdapter {

	final int PAGE_COUNT = 4;
	// Tab Titles
	private String tabtitles[] = new String[] { "24 Hours", "24 Days",
			"12 Months", "12 Years" };
	Context context;



	



	ArrayList<HashMap<String, String>> data;

	Cursor c;

	public ConsuptionViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {

		case 0:
			Chart24Hour chart24Hour = new Chart24Hour();
			return chart24Hour;
		case 1:
			Chart24Days chart24Days = new Chart24Days();
			return chart24Days;
		case 2:
			Chart12Months chart12Months = new Chart12Months();
			return chart12Months;
		case 3:
			Chart12Years chart12Years = new Chart12Years();
			return chart12Years;
		}
		return null;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabtitles[position];
	}

	public static double[] convertToDoubleArray(String[] array) {
		double[] tempArray = new double[array.length];
		for (int i = 0; i < array.length; i++) {
			if (Double.parseDouble(array[i]) < 0.01) {
				tempArray[i] = MathHelper.NULL_VALUE;
			} else {
				tempArray[i] = Double.parseDouble(array[i]);
			}
		}
		return tempArray;
	}

	static public class Chart24Hour extends Fragment {
		private BarChart mChart;
		private Switch mySwitch;
		BarDataSet set1;
		public static final int TEXT_SIZE_XHDPI = 15;
		public static final int TEXT_SIZE_HDPI = 12;
		public static final int TEXT_SIZE_MDPI = 11;
		public static final int TEXT_SIZE_LDPI = 10;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			mySwitch.setChecked(true);
		

			mChart.animateY(1200);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("Consumption of Heat And Hot Water 24 Hours");
			mChart.setDescriptionColor(Color.WHITE);
			mChart.setDescriptionPosition(15f, 15f);
			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

				@Override
				public void onValueSelected(Entry e, int dataSetIndex,
						Highlight h) {
				
					Toast.makeText(
							getActivity(),
							String.valueOf(+e.getVal() + ", xIndex: "
									+ e.getXIndex()), 1000).show();
				}

				@Override
				public void onNothingSelected() {
				

				}
			});

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			ArrayList<String> xVals = new ArrayList<String>();
			for (int i = 0; i < 25; i++) {
				xVals.add(String.valueOf(i));
			}

			ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
			  Calendar rightNow= Calendar.getInstance();     
			  
			  // rightNow.get(Calendar.MINUTE)
			  
			for (int i = 0; i <12; i++) {
				yVals1.add(new BarEntry(
						new float[] {
								Float.valueOf(ConsuptionActivitySwipe.totalHourArray[i]),
								Float.valueOf(ConsuptionActivitySwipe.dhwHourArray[i]) },
						i));
			}

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
			

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);
		

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);
			

				break;
			}

			mChart.invalidate();

			mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					

					if (isChecked) {

						Log.e("isChecked:", "true");

						loadLimitLines();
						set1.setValueTextColor(Color.WHITE);
						mChart.invalidate();

					} else {
						mChart.getAxisLeft().getLimitLines().clear();
						set1.setValueTextColor(Color.TRANSPARENT);
						mChart.invalidate();
						Log.e("isChecked:", "false");

					}

				}
			});

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivitySwipe.totalHourArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 24;

			LimitLine llHeatAxis = new LimitLine(heatAverage,
					"Average Heat Production");
			llHeatAxis.setLineWidth(2f);
	
			llHeatAxis.setLineColor(getResources().getColor(R.color.green));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			llHeatAxis.setTextColor(Color.WHITE);

			final YAxis leftAxis = mChart.getAxisLeft();

			
			leftAxis.addLimitLine(llHeatAxis);

			String[] splittedHotWaterValues = ConsuptionActivitySwipe.dhwHourArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 24;

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage,
					"Average Hot Water Production");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.YELLOW);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);
			llHotWaterAxis.setTextColor(Color.YELLOW);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			
			
			
			
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
		

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
			


				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
		

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
			

				break;
			}

			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}

	}

	static public class Chart24Days extends Fragment {
		private BarChart mChart;
		private Switch mySwitch;
		BarDataSet set1;
		

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			mySwitch.setChecked(true);
			// attach a listener to check for changes in state

			mChart.animateY(1200);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("Consumption of Heat And Hot Water 24 Days");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

				@Override
				public void onValueSelected(Entry e, int dataSetIndex,
						Highlight h) {
					// TODO Auto-generated method stub
					Toast.makeText(
							getActivity(),
							String.valueOf(+e.getVal() + ", xIndex: "
									+ e.getXIndex()), 1000).show();
				}

				@Override
				public void onNothingSelected() {
				

				}
			});

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
		
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			ArrayList<String> xVals = new ArrayList<String>();
			for (int i = 0; i < 24; i++) {
				xVals.add(String.valueOf(i));
			}

			ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

			for (int i = 0; i < ConsuptionActivitySwipe.totalDaysArray.length; i++) {
				yVals1.add(new BarEntry(
						new float[] {
								Float.valueOf(ConsuptionActivitySwipe.totalDaysArray[i]),
								Float.valueOf(ConsuptionActivitySwipe.dhwDaysArray[i]) },
						i));
			}

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
		

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);
	

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);
	

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);
			

				break;
			}

			mChart.invalidate();

			mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
	

					if (isChecked) {

						Log.e("isChecked:", "true");

						loadLimitLines();
						set1.setValueTextColor(Color.WHITE);
						mChart.invalidate();

					} else {
						mChart.getAxisLeft().getLimitLines().clear();
						set1.setValueTextColor(Color.TRANSPARENT);
						mChart.invalidate();
						Log.e("isChecked:", "false");

					}

				}
			});

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivitySwipe.totalDaysArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 24;

			LimitLine llHeatAxis = new LimitLine(heatAverage,
					"Average Heat Production");
			llHeatAxis.setLineWidth(2f);
			llHeatAxis.setLineColor(getResources().getColor(R.color.green));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			llHeatAxis.setTextColor(Color.WHITE);

			final YAxis leftAxis = mChart.getAxisLeft();

			
			leftAxis.addLimitLine(llHeatAxis);

			String[] splittedHotWaterValues = ConsuptionActivitySwipe.dhwDaysArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 24;

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage,
					"Average Hot Water Production");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.YELLOW);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);
			llHotWaterAxis.setTextColor(Color.YELLOW);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				
				

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

		}

	}

	static public class Chart12Months extends Fragment {
		private BarChart mChart;
		private Switch mySwitch;
		BarDataSet set1;
		

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			mySwitch.setChecked(true);
			// attach a listener to check for changes in state

			mChart.animateY(1200);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("Consumption of Heat And Hot Water 12 Months");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

				@Override
				public void onValueSelected(Entry e, int dataSetIndex,
						Highlight h) {
					// TODO Auto-generated method stub
					Toast.makeText(
							getActivity(),
							String.valueOf(+e.getVal() + ", xIndex: "
									+ e.getXIndex()), 1000).show();
				}

				@Override
				public void onNothingSelected() {
					// TODO Auto-generated method stub

				}
			});

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			ArrayList<String> xVals = new ArrayList<String>();
			for (int i = 1; i < 13; i++) {
				xVals.add(String.valueOf(i));
			}

			ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

			for (int i = 0; i < ConsuptionActivitySwipe.totalMonthsArray.length; i++) {
				yVals1.add(new BarEntry(
						new float[] {
								Float.valueOf(ConsuptionActivitySwipe.totalMonthsArray[i]),
								Float.valueOf(ConsuptionActivitySwipe.dhwMonthsArray[i]) },
						i));
			}

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

			mChart.invalidate();

			mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
		

					if (isChecked) {

						Log.e("isChecked:", "true");

						loadLimitLines();
						set1.setValueTextColor(Color.WHITE);
						mChart.invalidate();

					} else {
						mChart.getAxisLeft().getLimitLines().clear();
						set1.setValueTextColor(Color.TRANSPARENT);
						mChart.invalidate();
						Log.e("isChecked:", "false");

					}

				}
			});

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivitySwipe.totalMonthsArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 12;

			LimitLine llHeatAxis = new LimitLine(heatAverage,
					"Average Heat Production");
			llHeatAxis.setLineWidth(2f);
			llHeatAxis.setLineColor(getResources().getColor(R.color.green));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			llHeatAxis.setTextColor(Color.WHITE);

			final YAxis leftAxis = mChart.getAxisLeft();

			
			leftAxis.addLimitLine(llHeatAxis);

			String[] splittedHotWaterValues = ConsuptionActivitySwipe.dhwMonthsArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 12;

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage,
					"Average Hot Water Production");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.YELLOW);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);
			llHotWaterAxis.setTextColor(Color.YELLOW);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

		}

	}

	static public class Chart12Years extends Fragment {
		private BarChart mChart;
		private Switch mySwitch;
		BarDataSet set1;
	

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			mySwitch.setChecked(true);
			// attach a listener to check for changes in state

			mChart.animateY(1200);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("Consumption of Heat And Hot Water 12 Years");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

				@Override
				public void onValueSelected(Entry e, int dataSetIndex,
						Highlight h) {
					// TODO Auto-generated method stub
					Toast.makeText(
							getActivity(),
							String.valueOf(+e.getVal() + ", xIndex: "
									+ e.getXIndex()), 1000).show();
				}

				@Override
				public void onNothingSelected() {
					// TODO Auto-generated method stub

				}
			});

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			ArrayList<String> xVals = new ArrayList<String>();
			for (int i = 1; i < 13; i++) {
				xVals.add(String.valueOf(i));
			}

			ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

			for (int i = 0; i < ConsuptionActivitySwipe.totalYearsArray.length; i++) {
				yVals1.add(new BarEntry(
						new float[] {
								Float.valueOf(ConsuptionActivitySwipe.totalYearsArray[i]),
								Float.valueOf(ConsuptionActivitySwipe.dhwYearsArray[i]) },
						i));
			}

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
		

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);
	

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);
	

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);
			

				break;
			}

			mChart.invalidate();

			mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
				

					if (isChecked) {

						Log.e("isChecked:", "true");

						loadLimitLines();
						set1.setValueTextColor(Color.WHITE);
						mChart.invalidate();

					} else {
						mChart.getAxisLeft().getLimitLines().clear();
						set1.setValueTextColor(Color.TRANSPARENT);
						mChart.invalidate();
						Log.e("isChecked:", "false");

					}

				}
			});

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivitySwipe.totalYearsArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 12;

			LimitLine llHeatAxis = new LimitLine(heatAverage,
					"Average Heat Production");
			llHeatAxis.setLineWidth(2f);
			llHeatAxis.setLineColor(getResources().getColor(R.color.green));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			llHeatAxis.setTextColor(Color.WHITE);

			final YAxis leftAxis = mChart.getAxisLeft();

			

			leftAxis.addLimitLine(llHeatAxis);

			String[] splittedHotWaterValues = ConsuptionActivitySwipe.dhwYearsArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 24;

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage,
					"Average Hot Water Production");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.YELLOW);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);
			llHotWaterAxis.setTextColor(Color.YELLOW);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				llHotWaterAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				llHeatAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}
		}

	}

}