package com.kat.udpapp;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import pl.polidea.view.ZoomView;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.udpapp.R;
import com.kat.udpapp.chart.AbstractDemoChart;

public class ConsuptionActivity extends FragmentActivity implements
		OnClickListener {

	TextView txtHeaderTop, txtBack, txtHelp;

	Button btn24Hour, btn24Days, btn12Months, btn12Years;

	ScrollView scroll24Hour, scroll24Days, scroll12Months, scroll12Years;

	private String password, myIP;
	private String hotWaterProduction = "Hot Water Production ",
			heatProdution = "Heat Production ";

	private InetAddress controllerAddress;
	private InetAddress myAddress;
	private NBEControllerClient client;

	View v;

	LinearLayout main_container;
	private ZoomView zoomView;

	LinearLayout llBack;

	private GraphicalView mChartView24Hour, mChartView24Day, mChartView12Month,
			mChartView12Years;

	String[] totalHourArray = null, dhwHourArray = null;
	String[] totalDaysArray = null, dhwDaysArray = null;
	String[] totalMonthsArray = null, dhwMonthsArray = null;
	String[] totalYearsArray = null, dhwYearsArray = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		setContentView(R.layout.activity_second);

		v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.consumption_activity, null, false);
		v.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		password = "6464111183";
		try {
			controllerAddress = InetAddress.getByName("185.17.218.11");
			myIP = getLocalIpAddress();
			myAddress = InetAddress.getByName(myIP);
			client = new NBEControllerClient();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		txtHelp = (TextView) v.findViewById(R.id.txtHelp);
		txtBack = (TextView) v.findViewById(R.id.txtBack);
		txtHeaderTop = (TextView) v.findViewById(R.id.txtHeaderTop);
		llBack = (LinearLayout) v.findViewById(R.id.llBack);

		btn24Hour = (Button) v.findViewById(R.id.btn24Hour);
		btn24Days = (Button) v.findViewById(R.id.btn24Days);
		btn12Months = (Button) v.findViewById(R.id.btn12Months);
		btn12Years = (Button) v.findViewById(R.id.btn12Years);

		scroll24Hour = (ScrollView) v.findViewById(R.id.scroll24Hour);
		scroll24Days = (ScrollView) v.findViewById(R.id.scroll24Days);
		scroll12Months = (ScrollView) v.findViewById(R.id.scroll12Months);
		scroll12Years = (ScrollView) v.findViewById(R.id.scroll12Years);

		btn24Hour.setOnClickListener(ConsuptionActivity.this);
		btn24Days.setOnClickListener(ConsuptionActivity.this);
		btn12Months.setOnClickListener(ConsuptionActivity.this);
		btn12Years.setOnClickListener(ConsuptionActivity.this);

		scroll24Hour.setVisibility(View.VISIBLE);
		scroll24Days.setVisibility(View.GONE);
		scroll12Months.setVisibility(View.GONE);
		scroll12Years.setVisibility(View.GONE);

		new getConsuptionData().execute();

		llBack.setOnClickListener(ConsuptionActivity.this);

		SharedPreferences preferences = getSharedPreferences(
				Constants.language, Context.MODE_PRIVATE);

		txtBack.setText(preferences.getString("lng_button_back", ""));
		txtHelp.setText(preferences.getString("lng_button_help", ""));
		txtHeaderTop.setText(preferences
				.getString("lng_button_consumption", ""));

		zoomView = new ZoomView(ConsuptionActivity.this);
		zoomView.addView(v);

		// viewPager.setAdapter(new ConsuptionViewPagerAdapter(
		// getSupportFragmentManager()));

		try {
			main_container = (LinearLayout) findViewById(R.id.main_container);
			main_container.addView(zoomView);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getLocalIpAddress() {
		try {
			String ip = null;
			ConnectivityManager connManager = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			WifiManager wifiMan = (WifiManager) this
					.getSystemService(Context.WIFI_SERVICE);
			if (wifiMan.isWifiEnabled()) {
				WifiInfo wifiInf = wifiMan.getConnectionInfo();

				int ipAddress = wifiInf.getIpAddress();
				ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
						(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
						(ipAddress >> 24 & 0xff));
				return ip;
			} else if ((netInfo != null) && netInfo.isConnected()) {

				for (Enumeration<NetworkInterface> en = NetworkInterface
						.getNetworkInterfaces(); en.hasMoreElements();) {
					NetworkInterface intf = en.nextElement();
					for (Enumeration<InetAddress> enumIpAddr = intf
							.getInetAddresses(); enumIpAddr.hasMoreElements();) {
						InetAddress inetAddress = enumIpAddr.nextElement();
						if (!inetAddress.isLoopbackAddress()) {
							ip = inetAddress.getHostAddress().toString();
						}
					}
				}
				return ip;
			}

		} catch (Exception ex) {
			Log.e("IP Address", ex.toString());
		}

		return null;
	}

	public class getConsuptionData extends AsyncTask<Void, Void, Void> {

		ControllerResponse response;
		ControllerRequest cr;
		Map<String, String> map;
		boolean error = false;

		@Override
		protected Void doInBackground(Void... params) {
			try {
				error = false;
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_hours");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					totalHourArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_days");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					totalDaysArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_months");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					totalMonthsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_years");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					totalYearsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_hours");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					dhwHourArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_days");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					dhwDaysArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_months");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					dhwMonthsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_years");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " : "
							+ entry.getValue());
					dhwYearsArray = entry.getValue().split(",");
				}
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (error) {
				new getConsuptionData().execute();
			} else {
				Toast.makeText(ConsuptionActivity.this, "Complete", Toast.LENGTH_SHORT)
						.show();
			}
			if (totalHourArray != null && dhwHourArray != null) {
				loadChart24Hour();
			}
			if (totalDaysArray != null && dhwDaysArray != null) {
				loadChart24Days();
			}
			if (totalMonthsArray != null && dhwMonthsArray != null) {
				loadChart12Months();
			}
			if (totalYearsArray != null && dhwYearsArray != null) {
				loadChart12Years();
			}
			super.onPostExecute(result);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public void loadChart24Hour() {
		if (mChartView24Hour == null) {
			LinearLayout layout = (LinearLayout) v
					.findViewById(R.id.chart24Hour);
			String[] titles = new String[] { heatProdution, hotWaterProduction };
			List<double[]> values = new ArrayList<double[]>();
			values.add(convertToDoubleArray(totalHourArray));
			values.add(convertToDoubleArray(dhwHourArray));
			int[] colors = new int[] { Color.parseColor("#00FF00"),
					Color.parseColor("#013547") };
			XYMultipleSeriesRenderer renderer = AbstractDemoChart
					.buildBarRenderer(colors);
			AbstractDemoChart.setChartSettings(renderer,
					"Consuption for heat and hot water production.(24 Hours)",
					"", "", 0.5, 10, 0, 1.3, Color.WHITE, Color.WHITE);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(0))
					.setDisplayChartValues(true);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(1))
					.setDisplayChartValues(true);

			renderer.setApplyBackgroundColor(true);
			renderer.setBackgroundColor(Color.parseColor("#80000000"));
			int[] margins = new int[] {
					(int) getResources().getDimension(R.dimen.twozero),// top
					(int) getResources().getDimension(R.dimen.fivezero),// left
					(int) getResources().getDimension(R.dimen.twozero),// bottom
					(int) getResources().getDimension(R.dimen.threezero) // right
			};
			renderer.setMargins(margins);
			renderer.setMarginsColor(Color.parseColor("#8000698c"));
			renderer.setAxesColor(Color.WHITE);

			Calendar cal = Calendar.getInstance();
			int min = 0, max = 10;
			if (cal.get(Calendar.HOUR_OF_DAY) > 10) {
				min = cal.get(Calendar.HOUR_OF_DAY) - 10;
				max = cal.get(Calendar.HOUR_OF_DAY);
			}
			renderer.setXAxisMin(min);
			renderer.setXAxisMax(max);

			renderer.setLabelsColor(Color.WHITE);
			renderer.setBarSpacing(getResources().getDimension(R.dimen.five));
			renderer.setBarWidth(getResources().getDimension(R.dimen.twozero));

			renderer.setXLabels(12);
			renderer.setYLabels(10);
			renderer.setXLabelsAlign(Align.RIGHT);

			double[] panLimits = { 0, 25, 0, 2 };
			renderer.setPanLimits(panLimits);

			renderer.setXLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setYLabelsAlign(Align.RIGHT);
			renderer.setYLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setPanEnabled(true, false);
			renderer.setZoomEnabled(true);
			renderer.setZoomRate(1.1f);
			renderer.setBarSpacing(0.5f);

			mChartView24Hour = ChartFactory.getBarChartView(
					ConsuptionActivity.this,
					AbstractDemoChart.buildBarDataset(titles, values),
					renderer, Type.STACKED);
			layout.addView(mChartView24Hour, new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}

	private double[] convertToDoubleArray(String[] array) {
		double[] tempArray = new double[array.length];
		for (int i = 0; i < array.length; i++) {
			tempArray[i] = Double.parseDouble(array[i]);
		}
		return tempArray;
	}

	public void loadChart24Days() {
		if (mChartView24Day == null) {
			LinearLayout layout = (LinearLayout) v
					.findViewById(R.id.chart24Days);
			String[] titles = new String[] { heatProdution, hotWaterProduction };
			List<double[]> values = new ArrayList<double[]>();
			values.add(convertToDoubleArray(totalDaysArray));
			values.add(convertToDoubleArray(dhwDaysArray));
			int[] colors = new int[] { Color.parseColor("#00FF00"),
					Color.parseColor("#013547") };
			XYMultipleSeriesRenderer renderer = AbstractDemoChart
					.buildBarRenderer(colors);
			AbstractDemoChart.setChartSettings(renderer,
					"Consuption for heat and hot water production.(24 Days)",
					"", "", 0.5, 10, 0, 1.3, Color.WHITE, Color.WHITE);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(0))
					.setDisplayChartValues(true);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(1))
					.setDisplayChartValues(true);

			renderer.setApplyBackgroundColor(true);
			renderer.setBackgroundColor(Color.parseColor("#80000000"));
			int[] margins = new int[] {
					(int) getResources().getDimension(R.dimen.twozero),// top
					(int) getResources().getDimension(R.dimen.fivezero),// left
					(int) getResources().getDimension(R.dimen.twozero),// bottom
					(int) getResources().getDimension(R.dimen.threezero) // right
			};
			renderer.setMargins(margins);
			renderer.setMarginsColor(Color.parseColor("#8000698c"));
			renderer.setAxesColor(Color.WHITE);

			renderer.setLabelsColor(Color.WHITE);
			renderer.setBarSpacing(getResources().getDimension(R.dimen.five));
			renderer.setBarWidth(getResources().getDimension(R.dimen.twozero));

			renderer.setXLabels(12);
			renderer.setYLabels(10);
			renderer.setXLabelsAlign(Align.RIGHT);

			double[] panLimits = { 0, 32, 0, 2 };
			renderer.setPanLimits(panLimits);

			renderer.setXLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setYLabelsAlign(Align.RIGHT);
			renderer.setYLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setPanEnabled(true, false);
			renderer.setZoomEnabled(true);
			renderer.setZoomRate(1.1f);
			renderer.setBarSpacing(0.5f);

			mChartView24Day = ChartFactory.getBarChartView(
					ConsuptionActivity.this,
					AbstractDemoChart.buildBarDataset(titles, values),
					renderer, Type.STACKED);
			layout.addView(mChartView24Day, new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}

	public void loadChart12Months() {
		if (mChartView12Month == null) {
			LinearLayout layout = (LinearLayout) v
					.findViewById(R.id.chart12months);
			String[] titles = new String[] { heatProdution, hotWaterProduction };
			List<double[]> values = new ArrayList<double[]>();
			values.add(convertToDoubleArray(totalMonthsArray));
			values.add(convertToDoubleArray(dhwMonthsArray));
			int[] colors = new int[] { Color.parseColor("#00FF00"),
					Color.parseColor("#013547") };
			XYMultipleSeriesRenderer renderer = AbstractDemoChart
					.buildBarRenderer(colors);
			AbstractDemoChart.setChartSettings(renderer,
					"Consuption for heat and hot water production.(12 Months)",
					"", "", 0.5, 10, 0, 0.5, Color.WHITE, Color.WHITE);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(0))
					.setDisplayChartValues(true);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(1))
					.setDisplayChartValues(true);

			renderer.setApplyBackgroundColor(true);
			renderer.setBackgroundColor(Color.parseColor("#80000000"));
			int[] margins = new int[] {
					(int) getResources().getDimension(R.dimen.twozero),// top
					(int) getResources().getDimension(R.dimen.fivezero),// left
					(int) getResources().getDimension(R.dimen.twozero),// bottom
					(int) getResources().getDimension(R.dimen.threezero) // right
			};
			renderer.setMargins(margins);
			renderer.setMarginsColor(Color.parseColor("#8000698c"));
			renderer.setAxesColor(Color.WHITE);

			renderer.setLabelsColor(Color.WHITE);
			renderer.setBarSpacing(getResources().getDimension(R.dimen.five));
			renderer.setBarWidth(getResources().getDimension(R.dimen.twozero));

			renderer.addXTextLabel(1, "Jan");
			renderer.addXTextLabel(2, "Feb");
			renderer.addXTextLabel(3, "Mar");
			renderer.addXTextLabel(4, "Apr");
			renderer.addXTextLabel(5, "May");
			renderer.addXTextLabel(6, "Jun");
			renderer.addXTextLabel(7, "Jul");
			renderer.addXTextLabel(8, "Aug");
			renderer.addXTextLabel(9, "Sep");
			renderer.addXTextLabel(10, "Oct");
			renderer.addXTextLabel(11, "Nov");
			renderer.addXTextLabel(12, "Dec");

			double[] panLimits = { 0, 13, 0, 2 };
			renderer.setPanLimits(panLimits);

			renderer.setXLabels(0);
			renderer.setYLabels(10);
			renderer.setXLabelsAlign(Align.RIGHT);

			renderer.setXLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setYLabelsAlign(Align.RIGHT);
			renderer.setYLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setPanEnabled(true, false);
			renderer.setZoomEnabled(true);
			renderer.setZoomRate(1.1f);
			renderer.setBarSpacing(0.5f);

			mChartView12Month = ChartFactory.getBarChartView(
					ConsuptionActivity.this,
					AbstractDemoChart.buildBarDataset(titles, values),
					renderer, Type.STACKED);
			layout.addView(mChartView12Month, new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}

	public void loadChart12Years() {
		if (mChartView12Years == null) {
			LinearLayout layout = (LinearLayout) v
					.findViewById(R.id.chart12Years);
			String[] titles = new String[] { heatProdution, hotWaterProduction };
			List<double[]> values = new ArrayList<double[]>();
			values.add(convertToDoubleArray(totalYearsArray));
			values.add(convertToDoubleArray(dhwYearsArray));
			int[] colors = new int[] { Color.parseColor("#00FF00"),
					Color.parseColor("#013547") };
			XYMultipleSeriesRenderer renderer = AbstractDemoChart
					.buildBarRenderer(colors);
			AbstractDemoChart.setChartSettings(renderer,
					"Consuption for heat and hot water production.(12 Years)",
					"", "", 0.5, 10, 0, 0.5, Color.WHITE, Color.WHITE);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(0))
					.setDisplayChartValues(true);
			((XYSeriesRenderer) renderer.getSeriesRendererAt(1))
					.setDisplayChartValues(true);

			renderer.setApplyBackgroundColor(true);
			renderer.setBackgroundColor(Color.parseColor("#80000000"));
			int[] margins = new int[] {
					(int) getResources().getDimension(R.dimen.twozero),// top
					(int) getResources().getDimension(R.dimen.fivezero),// left
					(int) getResources().getDimension(R.dimen.twozero),// bottom
					(int) getResources().getDimension(R.dimen.threezero) // right
			};
			renderer.setMargins(margins);
			renderer.setMarginsColor(Color.parseColor("#8000698c"));
			renderer.setAxesColor(Color.WHITE);

			renderer.setLabelsColor(Color.WHITE);
			renderer.setBarSpacing(getResources().getDimension(R.dimen.five));
			renderer.setBarWidth(getResources().getDimension(R.dimen.twozero));

			for (int i = 1; i < 13; i++) {
				renderer.addXTextLabel(i, "20" + (i + 12));
			}

			double[] panLimits = { 0, 13, 0, 2 };
			renderer.setPanLimits(panLimits);

			renderer.setXLabels(0);
			renderer.setYLabels(10);
			renderer.setXLabelsAlign(Align.RIGHT);

			renderer.setXLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setYLabelsAlign(Align.RIGHT);
			renderer.setYLabelsPadding(getResources().getDimension(
					R.dimen.three));
			renderer.setPanEnabled(true, false);
			renderer.setZoomEnabled(true);
			renderer.setZoomRate(1.1f);
			renderer.setBarSpacing(0.5f);

			mChartView12Years = ChartFactory.getBarChartView(
					ConsuptionActivity.this,
					AbstractDemoChart.buildBarDataset(titles, values),
					renderer, Type.STACKED);
			layout.addView(mChartView12Years, new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}

	@Override
	public void onClick(View v) {
		if (v == llBack) {
			onBackPressed();
		} else if (v == btn24Hour) {
			scroll24Hour.setVisibility(View.VISIBLE);
			scroll24Days.setVisibility(View.GONE);
			scroll12Months.setVisibility(View.GONE);
			scroll12Years.setVisibility(View.GONE);
		} else if (v == btn24Days) {
			scroll24Hour.setVisibility(View.GONE);
			scroll24Days.setVisibility(View.VISIBLE);
			scroll12Months.setVisibility(View.GONE);
			scroll12Years.setVisibility(View.GONE);
		} else if (v == btn12Months) {
			scroll24Hour.setVisibility(View.GONE);
			scroll24Days.setVisibility(View.GONE);
			scroll12Months.setVisibility(View.VISIBLE);
			scroll12Years.setVisibility(View.GONE);
		} else if (v == btn12Years) {
			scroll24Hour.setVisibility(View.GONE);
			scroll24Days.setVisibility(View.GONE);
			scroll12Months.setVisibility(View.GONE);
			scroll12Years.setVisibility(View.VISIBLE);
		}
	}
}