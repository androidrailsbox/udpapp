package com.kat.udpapp;

import pl.polidea.view.ZoomView;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.udpapp.R;

public class SecondActivty extends Activity {

	private ZoomView zoomView;
	LinearLayout main_container;
	TextView mWeatherHeaderTextView, mLoginTextView, mMailTextView,
			mBoilerTextView, mHooperTextView, mDhwTextView, mWeatherTextView,
			mSunControlTextView, mConsumptionTextView, mChartsTextView,
			mDownloadTextView, mLogTextView, mSystemTextView, mScreenTextView,
			mManualTextView, mStoppedTempTextView, mOutput1TextView,
			mOutput2TextView, mOutput3TextView, mOutput4TextView,
			mOutput5TextView, mOutput6TextView, mOutput7TextView,
			mOutput8TextView, mOutput9TextView, mOutput10TextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_second);

		View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.mainview, null, false);
		v.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		zoomView = new ZoomView(this);
		zoomView.addView(v);

		main_container = (LinearLayout) findViewById(R.id.main_container);
		main_container.addView(zoomView);
		Typeface myTypeface = Typeface.createFromAsset(getAssets(),
				"OpenSans-Bold.ttf");

		mWeatherHeaderTextView = (TextView) findViewById(R.id.weather_header);
		mLoginTextView = (TextView) findViewById(R.id.txt_login);
		mMailTextView = (TextView) findViewById(R.id.txt_mail);
		mBoilerTextView = (TextView) findViewById(R.id.txt_boiler);
		mHooperTextView = (TextView) findViewById(R.id.txt_hooper);
		mDhwTextView = (TextView) findViewById(R.id.txt_dhw);
		mWeatherTextView = (TextView) findViewById(R.id.txt_weather);
		mSunControlTextView = (TextView) findViewById(R.id.txt_suncontrol);
		mConsumptionTextView = (TextView) findViewById(R.id.txt_compustion);
		mChartsTextView = (TextView) findViewById(R.id.txt_charts);
		mDownloadTextView = (TextView) findViewById(R.id.txt_download);
		mLogTextView = (TextView) findViewById(R.id.txt_log);
		mSystemTextView = (TextView) findViewById(R.id.txt_system);
		mScreenTextView = (TextView) findViewById(R.id.txt_screen);
		mManualTextView = (TextView) findViewById(R.id.txt_manual);
		mStoppedTempTextView = (TextView) findViewById(R.id.stopped_reached_temp);
		mOutput1TextView = (TextView) findViewById(R.id.output1);
		mOutput2TextView = (TextView) findViewById(R.id.output2);
		mOutput3TextView = (TextView) findViewById(R.id.output3);
		mOutput4TextView = (TextView) findViewById(R.id.output4);
		mOutput5TextView = (TextView) findViewById(R.id.output5);
		mOutput6TextView = (TextView) findViewById(R.id.output6);
		mOutput7TextView = (TextView) findViewById(R.id.output7);
		mOutput8TextView = (TextView) findViewById(R.id.output8);
		mOutput9TextView = (TextView) findViewById(R.id.output9);
		mOutput10TextView = (TextView) findViewById(R.id.output10);

		mWeatherHeaderTextView.setTypeface(myTypeface);
		mLoginTextView.setTypeface(myTypeface);
		mMailTextView.setTypeface(myTypeface);
		mBoilerTextView.setTypeface(myTypeface);
		mHooperTextView.setTypeface(myTypeface);
		mDhwTextView.setTypeface(myTypeface);
		mWeatherTextView.setTypeface(myTypeface);
		mSunControlTextView.setTypeface(myTypeface);
		mConsumptionTextView.setTypeface(myTypeface);
		mChartsTextView.setTypeface(myTypeface);
		mDownloadTextView.setTypeface(myTypeface);
		mLogTextView.setTypeface(myTypeface);
		mSystemTextView.setTypeface(myTypeface);
		mScreenTextView.setTypeface(myTypeface);
		mManualTextView.setTypeface(myTypeface);
		mOutput1TextView.setTypeface(myTypeface);
		mOutput2TextView.setTypeface(myTypeface);
		mOutput3TextView.setTypeface(myTypeface);
		mOutput4TextView.setTypeface(myTypeface);
		mOutput5TextView.setTypeface(myTypeface);
		mOutput6TextView.setTypeface(myTypeface);
		mOutput7TextView.setTypeface(myTypeface);
		mOutput8TextView.setTypeface(myTypeface);
		mOutput9TextView.setTypeface(myTypeface);
		mOutput10TextView.setTypeface(myTypeface);

	}

}
