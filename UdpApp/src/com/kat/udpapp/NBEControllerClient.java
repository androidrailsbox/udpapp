/**
 *   This file is part of NBE Controller Api.
 *
 *   NBE Controller Api is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   NBE Controller Api is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NBE Controller Api.  If not, see <http://www.gnu.org/licenses/>.
 *   
 *   NBEControllerClient is a synchronous client for accessing a NBE controlbox.
 *   The client is compatible with never versions of v7, v10, RTB and v13 controllers. 
 *   
 *   For asynchronous access, use a threaded wrapper.
 */

/**
 * TODO:
 * - Add timeout on discovery and send, right now they timeout forever
 */

package com.kat.udpapp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.kat.udpapp.exception.ParseException;

public class NBEControllerClient {

	public static final int DEFAULT_TTL = 4; // Number of hops
												// (routers/equipment before the
												// message is discarded)
	public static final int DEFAULT_TIMEOUT = 5000;
	public static final int DEFAULT_DISCOVERY_TIMEOUT = 3000;
	public final static String BROADCAST_IP = "255.255.255.255"; // upnp/simple
																	// service
																	// discovery
																	// at
																	// "239.255.255.250";
	public final static int CONTROLLER_PORT = 1900; // The port of the control
													// box
	public final static int CLIENT_PORT = 1901; // The receiver port for this
												// program (for getting
												// responses)

	/**
	 * Get all ip addresses on all interfaces on this machine.
	 * 
	 * @return
	 * @throws UnknownHostException
	 */
	protected List<InetAddress> getLocalAddresses() {
		List<InetAddress> list = new ArrayList<InetAddress>();

		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface
					.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface iface = interfaces.nextElement();

				// filters out 127.0.0.1 and inactive interfaces
				if (iface.isLoopback() || !iface.isUp()) {
					continue;
				}

				Enumeration<InetAddress> addresses = iface.getInetAddresses();
				while (addresses.hasMoreElements()) {
					InetAddress addr = addresses.nextElement();
					list.add(addr);
				}
			}
		} catch (SocketException e) {
			throw new RuntimeException(e);
		}

		return list;
	}

	/**
	 * Get a list of controllers on any network interface. Assumes that
	 * controllers are available on one network at a time only. Refactor, to get
	 * more controllers to work on multiple different networks
	 * 
	 * @return
	 * @throws IOException
	 */
	public List<ControllerResponse> discoverControllers() throws IOException,
			ParseException {
		List<InetAddress> list = getLocalAddresses();
		List<ControllerResponse> controllerList;
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Searching: " + list.get(i));
			controllerList = discoverControllers(list.get(i));
			if (controllerList.size() > 0) {
				return controllerList;
			}
		}
		return null;
	}

	/**
	 * Find available controllers in the network. Refactor, to get it to return
	 * more than one.
	 * 
	 * @return
	 * @throws IOException
	 */
	public synchronized List<ControllerResponse> discoverControllers(InetAddress addr)
			throws IOException, ParseException {
		InetSocketAddress adr = new InetSocketAddress(
				InetAddress.getByName(BROADCAST_IP), CONTROLLER_PORT);

		java.net.MulticastSocket skt = new java.net.MulticastSocket(null);
		if (skt.isBound()) {

		} else {
			skt.bind(new InetSocketAddress(addr, CLIENT_PORT));
		}
		skt.setTimeToLive(DEFAULT_TTL);
		skt.setSoTimeout(DEFAULT_DISCOVERY_TIMEOUT);

		boolean timeout = false;
		int counter = 0;
		List<ControllerResponse> list = new ArrayList<ControllerResponse>();
		
		ControllerRequest cr = new ControllerRequestImpl();
		cr.setFunctionId(0);
		cr.setCode("0000000000");
		cr.setPayload(new String("NBE Discovery").getBytes());
		byte[] pk = cr.getRawRequest();
		skt.send(new DatagramPacket(pk, pk.length, adr));
		
		int netFail = 0;
		while (!timeout && netFail<8) {
			try {		
				byte receiveData[] = new byte[1500];
				DatagramPacket receivePacket = new DatagramPacket(receiveData,
						receiveData.length);
				long startTime = System.nanoTime();
				skt.receive(receivePacket);
				long endTime = System.nanoTime();
		
				System.out.println(new String(receivePacket.getData()));
		
				ControllerResponse cres = new ControllerResponseImpl();
				cres.setData(receivePacket.getData());
				cres.setReceiveTime(endTime - startTime);
				cres.setSenderAddress(addr);
				list.add(cres);
				if (counter++>50) { // Do not loop forever if no exception is thrown
					timeout = true;
				}
			} catch (SocketTimeoutException e) {
				timeout = true;
			} catch (Exception e) {
				netFail++; 
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
				}
			}
		} 

		skt.disconnect();
		skt.close();

		return list;
	}

	public synchronized ControllerResponse sendRequest(InetAddress senderAddr,
			InetAddress receiverAddr, ControllerRequest request)
			throws IOException, ParseException {
		InetSocketAddress adr = new InetSocketAddress(receiverAddr,
				CONTROLLER_PORT);

		java.net.DatagramSocket skt = new java.net.DatagramSocket(null);

		skt.bind(new InetSocketAddress(senderAddr, CLIENT_PORT));
		skt.setSoTimeout(DEFAULT_TIMEOUT);

		byte[] pk = request.getRawRequest();

		skt.send(new DatagramPacket(pk, pk.length, adr));

		byte receiveData[] = new byte[1500];
		DatagramPacket receivePacket = new DatagramPacket(receiveData,
				receiveData.length);
		long startTime = System.nanoTime();
		skt.receive(receivePacket);
		long endTime = System.nanoTime();

		ControllerResponse cres = new ControllerResponseImpl();
		cres.setData(receivePacket.getData());
		cres.setReceiveTime(endTime - startTime);
		cres.setSenderAddress(senderAddr);

		skt.disconnect();
		skt.close();

		return cres;
	}

	public static void main(String args[]) {
	}
}
